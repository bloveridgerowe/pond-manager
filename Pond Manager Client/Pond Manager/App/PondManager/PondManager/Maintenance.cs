﻿using System;

namespace PondManager
{
    [Serializable]
    internal class Maintenance
    {
        private DateTime
            mFilterClean,
            mPumpClean,
            mUvBulb,
            mWaterChange,
            mWaterTest;

        internal DateTime WaterChange
        {
            get => mWaterChange;
            set => mWaterChange = value;
        }

        internal DateTime WaterTest
        {
            get => mWaterTest;
            set => mWaterTest = value;
        }

        internal DateTime FilterClean
        {
            get => mFilterClean;
            set => mFilterClean = value;
        }

        internal DateTime PumpClean
        {
            get => mPumpClean;
            set => mPumpClean = value;
        }

        internal DateTime UvBulb
        {
            get => mUvBulb;
            set => mUvBulb = value;
        }
    }
}