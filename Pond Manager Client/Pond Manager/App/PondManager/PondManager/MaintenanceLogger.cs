﻿using System;
using System.Windows.Forms;

namespace PondManager
{
    partial class MaintenanceLogger : Form
    {
        private Maintenance mAintenance = new Maintenance();

        public Maintenance Maintenance
        {
            get => mAintenance;
            set => mAintenance = value;

        }

        public MaintenanceLogger()
        {
            InitializeComponent();
        }


        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void filterCleanedBT_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Filter clean logged");

            var now = DateTime.Now;

            foreach (Control c in filterCleanGB.Controls)
                if (c is RadioButton)
                {
                    var radio = c as RadioButton;

                    if (radio.Checked)
                        switch (radio.Text)
                        {
                            case "Weekly":
                                Maintenance.FilterClean = now.AddDays(7);
                                break;
                            case "Bimonthly":
                                Maintenance.FilterClean = now.AddDays(14);
                                break;
                            case "Monthly":
                                Maintenance.FilterClean = now.AddMonths(1);
                                break;
                        }
                }
        }

        private void pumpCleanedBT_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pump clean logged");

            var now = DateTime.Now;

            foreach (Control c in pumpCleanGB.Controls)
                if (c is RadioButton)
                {
                    var radio = c as RadioButton;

                    if (radio.Checked)
                        switch (radio.Text)
                        {
                            case "Monthly":
                                Maintenance.PumpClean = now.AddMonths(1);
                                break;
                            case "3 Months":
                                Maintenance.PumpClean = now.AddMonths(3);
                                break;
                            case "6 Months":
                                Maintenance.PumpClean = now.AddMonths(6);
                                break;
                        }
                }
        }

        private void UVreplacedBT_Click(object sender, EventArgs e)
        {
            MessageBox.Show("UV bulb replacement logged");

            var now = DateTime.Now;

            foreach (Control c in UVcleanGB.Controls)
                if (c is RadioButton)
                {
                    var radio = c as RadioButton;

                    if (radio.Checked)
                        switch (radio.Text)
                        {
                            case "6 Months":
                                Maintenance.UvBulb = now.AddMonths(6);
                                break;
                            case "9 Months":
                                Maintenance.UvBulb = now.AddMonths(9);
                                break;
                            case "12 Months":
                                Maintenance.UvBulb = now.AddMonths(12);
                                break;
                        }
                }
        }

        private void waterChangeBT_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Water change logged");

            var now = DateTime.Now;

            foreach (Control c in waterChangeGB.Controls)
                if (c is RadioButton)
                {
                    var radio = c as RadioButton;

                    if (radio.Checked)
                        switch (radio.Text)
                        {
                            case "Bimonthly":
                                Maintenance.WaterChange = now.AddDays(14);
                                break;
                            case "Monthly":
                                Maintenance.WaterChange = now.AddMonths(1);
                                break;
                            case "3 Months":
                                Maintenance.WaterChange = now.AddMonths(3);
                                break;
                        }
                }
        }

        private void waterTestBT_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Water test logged");

            var now = DateTime.Now;

            foreach (Control c in waterTestGB.Controls)
                if (c is RadioButton)
                {
                    var radio = c as RadioButton;

                    if (radio.Checked)
                        switch (radio.Text)
                        {
                            case "Weekly":
                                Maintenance.WaterTest = now.AddDays(7);
                                break;
                            case "Bimonthly":
                                Maintenance.WaterTest = now.AddDays(14);
                                break;
                            case "Monthly":
                                Maintenance.WaterTest = now.AddMonths(1);
                                break;
                        }
                }
        }
    }
}
