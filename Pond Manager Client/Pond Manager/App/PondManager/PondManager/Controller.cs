﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PondManager
{
    public static class Controller
    {
        private static TcpClient clientSocket = new TcpClient();
        private static NetworkStream serverStream;
        public static string IPAddress;
        public static int PortNo;

        private static string SendCommand(string command)
        {
            try
            {
                clientSocket = new TcpClient {ReceiveTimeout = 5000, SendTimeout = 5000};
                clientSocket.Connect(IP, Port);
                serverStream = clientSocket.GetStream();

                var outStream = Encoding.ASCII.GetBytes(command + "$");
                serverStream.Write(outStream, 0, outStream.Length); //send raw byte data using TCP/IP
                serverStream.Flush(); //ensure all data is sent

                var inStream = new byte[clientSocket.ReceiveBufferSize];
                serverStream.Read(inStream, 0,
                    clientSocket.ReceiveBufferSize); //fill the array with raw byte data received
                TrimEnd(ref inStream);
                var returnData = Encoding.ASCII.GetString(inStream);

                clientSocket.Close();
                return returnData;
            }
            catch
            {
                clientSocket.Close();
                return null;
            }
        }

        public static double WaterTemperature()
        {
            var buf = SendCommand("t");
            return buf == null ? 0 : Double.Parse(buf);
        }

        public static double pH()
        {
            var buf = SendCommand("p");
            return buf == null ? 0 : Double.Parse(buf);
        }

        public static string LocalTime()
        {
            return SendCommand("lt");
        }

        public static bool Feed(int feedTime)
        {
            var feedCommand = "f" + feedTime;
            return SendCommand(feedCommand) == feedCommand;
        }

        public static bool UpdateDevices(string deviceState)
        {
            var deviceCommand = "d" + deviceState;
            return SendCommand(deviceCommand) == deviceCommand;
        }

        public static bool Available()
        {
            var deviceCommmand = "a";
            var response = SendCommand(deviceCommmand);
            if (response == null) return false;
            return response == "a";
        }

        private static void TrimEnd(ref byte[] array)
        {
            var lastIndex = Array.FindLastIndex(array, b => b != 0);
            Array.Resize(ref array, lastIndex + 1);
        }

        public static string IP
        {
            get => IPAddress;
            set => IPAddress = value;
        }
        public static int Port
        {
            get => PortNo;
            set => PortNo = value;
        }
    }
}