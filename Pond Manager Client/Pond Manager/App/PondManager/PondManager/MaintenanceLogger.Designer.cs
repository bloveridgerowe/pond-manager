﻿namespace PondManager
{
    partial class MaintenanceLogger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintenanceLogger));
            this.filterCleanGB = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.filterCleanedBT = new System.Windows.Forms.Button();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.pumpCleanGB = new System.Windows.Forms.GroupBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pumpCleanedBT = new System.Windows.Forms.Button();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.UVcleanGB = new System.Windows.Forms.GroupBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.UVreplacedBT = new System.Windows.Forms.Button();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.waterChangeGB = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.waterChangeBT = new System.Windows.Forms.Button();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.waterTestGB = new System.Windows.Forms.GroupBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.waterTestBT = new System.Windows.Forms.Button();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.filterCleanGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.pumpCleanGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.UVcleanGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.waterChangeGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.waterTestGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // filterCleanGB
            // 
            this.filterCleanGB.Controls.Add(this.pictureBox9);
            this.filterCleanGB.Controls.Add(this.filterCleanedBT);
            this.filterCleanGB.Controls.Add(this.radioButton3);
            this.filterCleanGB.Controls.Add(this.radioButton2);
            this.filterCleanGB.Controls.Add(this.radioButton1);
            this.filterCleanGB.Location = new System.Drawing.Point(12, 199);
            this.filterCleanGB.Name = "filterCleanGB";
            this.filterCleanGB.Size = new System.Drawing.Size(280, 88);
            this.filterCleanGB.TabIndex = 1;
            this.filterCleanGB.TabStop = false;
            this.filterCleanGB.Text = "Filter";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::PondManager.Properties.Resources.filter;
            this.pictureBox9.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox9.Location = new System.Drawing.Point(20, 30);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(32, 32);
            this.pictureBox9.TabIndex = 17;
            this.pictureBox9.TabStop = false;
            // 
            // filterCleanedBT
            // 
            this.filterCleanedBT.Location = new System.Drawing.Point(175, 30);
            this.filterCleanedBT.Name = "filterCleanedBT";
            this.filterCleanedBT.Size = new System.Drawing.Size(75, 38);
            this.filterCleanedBT.TabIndex = 4;
            this.filterCleanedBT.Text = "Clean Performed";
            this.filterCleanedBT.UseVisualStyleBackColor = true;
            this.filterCleanedBT.Click += new System.EventHandler(this.filterCleanedBT_Click);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(71, 62);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(62, 17);
            this.radioButton3.TabIndex = 3;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Monthly";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(71, 41);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(70, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Bimonthly";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(71, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(61, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Weekly";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // pumpCleanGB
            // 
            this.pumpCleanGB.Controls.Add(this.pictureBox8);
            this.pumpCleanGB.Controls.Add(this.pumpCleanedBT);
            this.pumpCleanGB.Controls.Add(this.radioButton4);
            this.pumpCleanGB.Controls.Add(this.radioButton5);
            this.pumpCleanGB.Controls.Add(this.radioButton6);
            this.pumpCleanGB.Location = new System.Drawing.Point(12, 106);
            this.pumpCleanGB.Name = "pumpCleanGB";
            this.pumpCleanGB.Size = new System.Drawing.Size(280, 88);
            this.pumpCleanGB.TabIndex = 5;
            this.pumpCleanGB.TabStop = false;
            this.pumpCleanGB.Text = "Pump";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::PondManager.Properties.Resources.pump;
            this.pictureBox8.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox8.Location = new System.Drawing.Point(20, 36);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(32, 32);
            this.pictureBox8.TabIndex = 16;
            this.pictureBox8.TabStop = false;
            // 
            // pumpCleanedBT
            // 
            this.pumpCleanedBT.Location = new System.Drawing.Point(175, 30);
            this.pumpCleanedBT.Name = "pumpCleanedBT";
            this.pumpCleanedBT.Size = new System.Drawing.Size(75, 38);
            this.pumpCleanedBT.TabIndex = 4;
            this.pumpCleanedBT.Text = "Clean Performed";
            this.pumpCleanedBT.UseVisualStyleBackColor = true;
            this.pumpCleanedBT.Click += new System.EventHandler(this.pumpCleanedBT_Click);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(71, 62);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(69, 17);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "6 Months";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(71, 41);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(69, 17);
            this.radioButton5.TabIndex = 2;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "3 Months";
            this.radioButton5.UseVisualStyleBackColor = true;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Checked = true;
            this.radioButton6.Location = new System.Drawing.Point(71, 19);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(62, 17);
            this.radioButton6.TabIndex = 1;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Monthly";
            this.radioButton6.UseVisualStyleBackColor = true;
            // 
            // UVcleanGB
            // 
            this.UVcleanGB.Controls.Add(this.pictureBox7);
            this.UVcleanGB.Controls.Add(this.UVreplacedBT);
            this.UVcleanGB.Controls.Add(this.radioButton7);
            this.UVcleanGB.Controls.Add(this.radioButton8);
            this.UVcleanGB.Controls.Add(this.radioButton9);
            this.UVcleanGB.Location = new System.Drawing.Point(12, 294);
            this.UVcleanGB.Name = "UVcleanGB";
            this.UVcleanGB.Size = new System.Drawing.Size(280, 88);
            this.UVcleanGB.TabIndex = 6;
            this.UVcleanGB.TabStop = false;
            this.UVcleanGB.Text = "UV Bulb";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::PondManager.Properties.Resources.uv;
            this.pictureBox7.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox7.Location = new System.Drawing.Point(20, 30);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(32, 32);
            this.pictureBox7.TabIndex = 15;
            this.pictureBox7.TabStop = false;
            // 
            // UVreplacedBT
            // 
            this.UVreplacedBT.Location = new System.Drawing.Point(175, 30);
            this.UVreplacedBT.Name = "UVreplacedBT";
            this.UVreplacedBT.Size = new System.Drawing.Size(75, 38);
            this.UVreplacedBT.TabIndex = 4;
            this.UVreplacedBT.Text = "Bulb Replaced";
            this.UVreplacedBT.UseVisualStyleBackColor = true;
            this.UVreplacedBT.Click += new System.EventHandler(this.UVreplacedBT_Click);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(71, 62);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(75, 17);
            this.radioButton7.TabIndex = 3;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "12 Months";
            this.radioButton7.UseVisualStyleBackColor = true;
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(71, 41);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(69, 17);
            this.radioButton8.TabIndex = 2;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "9 Months";
            this.radioButton8.UseVisualStyleBackColor = true;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Checked = true;
            this.radioButton9.Location = new System.Drawing.Point(71, 19);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(69, 17);
            this.radioButton9.TabIndex = 1;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "6 Months";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // waterChangeGB
            // 
            this.waterChangeGB.Controls.Add(this.pictureBox1);
            this.waterChangeGB.Controls.Add(this.waterChangeBT);
            this.waterChangeGB.Controls.Add(this.radioButton10);
            this.waterChangeGB.Controls.Add(this.radioButton11);
            this.waterChangeGB.Controls.Add(this.radioButton12);
            this.waterChangeGB.Location = new System.Drawing.Point(12, 12);
            this.waterChangeGB.Name = "waterChangeGB";
            this.waterChangeGB.Size = new System.Drawing.Size(280, 88);
            this.waterChangeGB.TabIndex = 18;
            this.waterChangeGB.TabStop = false;
            this.waterChangeGB.Text = "Water Change";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PondManager.Properties.Resources.waterchange;
            this.pictureBox1.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox1.Location = new System.Drawing.Point(20, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // waterChangeBT
            // 
            this.waterChangeBT.Location = new System.Drawing.Point(175, 30);
            this.waterChangeBT.Name = "waterChangeBT";
            this.waterChangeBT.Size = new System.Drawing.Size(75, 38);
            this.waterChangeBT.TabIndex = 4;
            this.waterChangeBT.Text = "Water Changed";
            this.waterChangeBT.UseVisualStyleBackColor = true;
            this.waterChangeBT.Click += new System.EventHandler(this.waterChangeBT_Click);
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(71, 62);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(69, 17);
            this.radioButton10.TabIndex = 3;
            this.radioButton10.Text = "3 Months";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(71, 41);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(62, 17);
            this.radioButton11.TabIndex = 2;
            this.radioButton11.Text = "Monthly";
            this.radioButton11.UseVisualStyleBackColor = true;
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Checked = true;
            this.radioButton12.Location = new System.Drawing.Point(71, 19);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(70, 17);
            this.radioButton12.TabIndex = 1;
            this.radioButton12.TabStop = true;
            this.radioButton12.Text = "Bimonthly";
            this.radioButton12.UseVisualStyleBackColor = true;
            // 
            // waterTestGB
            // 
            this.waterTestGB.Controls.Add(this.pictureBox2);
            this.waterTestGB.Controls.Add(this.waterTestBT);
            this.waterTestGB.Controls.Add(this.radioButton13);
            this.waterTestGB.Controls.Add(this.radioButton14);
            this.waterTestGB.Controls.Add(this.radioButton15);
            this.waterTestGB.Location = new System.Drawing.Point(12, 391);
            this.waterTestGB.Name = "waterTestGB";
            this.waterTestGB.Size = new System.Drawing.Size(280, 90);
            this.waterTestGB.TabIndex = 18;
            this.waterTestGB.TabStop = false;
            this.waterTestGB.Text = "Water Test";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PondManager.Properties.Resources.watertest;
            this.pictureBox2.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox2.Location = new System.Drawing.Point(20, 30);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // waterTestBT
            // 
            this.waterTestBT.Location = new System.Drawing.Point(175, 30);
            this.waterTestBT.Name = "waterTestBT";
            this.waterTestBT.Size = new System.Drawing.Size(75, 38);
            this.waterTestBT.TabIndex = 4;
            this.waterTestBT.Text = "Water Tested";
            this.waterTestBT.UseVisualStyleBackColor = true;
            this.waterTestBT.Click += new System.EventHandler(this.waterTestBT_Click);
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Location = new System.Drawing.Point(71, 62);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(62, 17);
            this.radioButton13.TabIndex = 3;
            this.radioButton13.TabStop = true;
            this.radioButton13.Text = "Monthly";
            this.radioButton13.UseVisualStyleBackColor = true;
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Location = new System.Drawing.Point(71, 41);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(70, 17);
            this.radioButton14.TabIndex = 2;
            this.radioButton14.TabStop = true;
            this.radioButton14.Text = "Bimonthly";
            this.radioButton14.UseVisualStyleBackColor = true;
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Checked = true;
            this.radioButton15.Location = new System.Drawing.Point(71, 19);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(61, 17);
            this.radioButton15.TabIndex = 1;
            this.radioButton15.TabStop = true;
            this.radioButton15.Text = "Weekly";
            this.radioButton15.UseVisualStyleBackColor = true;
            // 
            // MaintenanceLogger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 495);
            this.Controls.Add(this.waterTestGB);
            this.Controls.Add(this.waterChangeGB);
            this.Controls.Add(this.UVcleanGB);
            this.Controls.Add(this.pumpCleanGB);
            this.Controls.Add(this.filterCleanGB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MaintenanceLogger";
            this.Text = "Reminders";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.filterCleanGB.ResumeLayout(false);
            this.filterCleanGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.pumpCleanGB.ResumeLayout(false);
            this.pumpCleanGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.UVcleanGB.ResumeLayout(false);
            this.UVcleanGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.waterChangeGB.ResumeLayout(false);
            this.waterChangeGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.waterTestGB.ResumeLayout(false);
            this.waterTestGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox filterCleanGB;
        private System.Windows.Forms.Button filterCleanedBT;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox pumpCleanGB;
        private System.Windows.Forms.Button pumpCleanedBT;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.GroupBox UVcleanGB;
        private System.Windows.Forms.Button UVreplacedBT;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.GroupBox waterChangeGB;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button waterChangeBT;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.GroupBox waterTestGB;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button waterTestBT;
        private System.Windows.Forms.RadioButton radioButton13;
        private System.Windows.Forms.RadioButton radioButton14;
        private System.Windows.Forms.RadioButton radioButton15;
    }
}