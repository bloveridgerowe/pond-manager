﻿using System;
using System.IO;
using System.Windows.Forms;

namespace PondManager
{
    public partial class LoadPond : Form
    {
        private string[] saveFiles;
        private string directory;

        public LoadPond()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            InitializeComponent();
            FindAvailableSaves();
            CheckFishIndexFile();
        }

        private void FindAvailableSaves()
        {
            directory = Directory.GetCurrentDirectory();
            saveFiles = Directory.GetFiles(directory, "*.bin");
            savesList.Items.Clear();

            foreach (var s in saveFiles)
            {
                var name = Path.GetFileName(s);
                if (name != null)
                {
                    name = name.Remove(name.Length - 4);
                    savesList.Items.Add(name);
                }
            }
        }

        private void CheckFishIndexFile()
        {
            if (Fish.PopulateIndexList()) return;
            MessageBox.Show("Formatting error in Fish Index file... Please repair and relaunch the application.");
            Environment.Exit(1);
        }

        private void LoadBT_Click(object sender, EventArgs e)
        {
            try
            {
                if (savesList.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a file...");
                }
                else
                {
                    var f = new MainWindow(saveFiles[savesList.SelectedIndex]);
                    f.Show();
                }
            }
            catch
            {
            }

            FindAvailableSaves();
        }

        private void NewBT_Click(object sender, EventArgs e)
        {
            var f = new MainWindow(directory + "\\" + pondNameTb.Text + ".bin");
            f.Show();
            pondNameTb.Text = "";
            FindAvailableSaves();
        }

        private void RemovePondBT_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you'd like to delete this profile?",
                "Delete Pond", MessageBoxButtons.YesNo);

            if (result == DialogResult.No) return;

            try
            {
                if (savesList.SelectedIndex == -1) MessageBox.Show("Please select a file...");
                else File.Delete(saveFiles[savesList.SelectedIndex]);
            }
            catch
            {
            }

            FindAvailableSaves();
        }
    }
}