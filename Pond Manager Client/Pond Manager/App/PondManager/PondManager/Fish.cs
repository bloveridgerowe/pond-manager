﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PondManager
{
    [Serializable]
    internal class Fish
    {
        private static Dictionary<string, double> indexList = new Dictionary<string, double>();
        private int age;
        private double size, bioload;
        private string
            comments,
            gender,
            species;

        public Fish(string species, double size, string gender, int age, string comments)
        {
            this.species = species;
            this.size = size;
            this.gender = gender;
            this.age = age;
            this.comments = comments;
            this.bioload = size * indexList[species];
        }

        public string Species
        {
            get => species;
            set => species = value;
        }

        public string Gender
        {
            get => gender;
            set => gender = value;
        }

        public int Age
        {
            get => age;
            set => age = value;
        }

        public string Comments
        {
            get => comments;
            set => comments = value;
        }

        public static Dictionary<string, double> IndexList
        {
            get => indexList;
            set => indexList = value;
        }

        public double Size
        {
            get => size;
            set => size = value;
        }

        public double Bioload
        {
            get => bioload;
            set => bioload = value;
        }

        public static bool PopulateIndexList()
        {
            indexList.Clear();

            try
            {
                using (var reader = new StreamReader("Data.csv"))
                {
                    var line = reader.ReadLine();

                    while (line != null)
                    {
                        var columns = line.Split(',');
                        indexList.Add(columns[0], double.Parse(columns[1]));
                        line = reader.ReadLine();
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}