﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace PondManager
{
    [Serializable]
    internal class Pond
    {
        private Maintenance maintenance = new Maintenance();
        private bool
            morningFeedComplete,
            eveningFeedComplete,
            additionalFeed;
        private int
            port = 9999,
            feedDuration = 1000;
        private string
            IPAddress = "127.0.0.1",
            morningFeedTime = "10:00",
            eveningFeedTime = "17:00",
            localPondTime;
        private MetaData metaData = new MetaData();
        private BindingList<Device> deviceList = new BindingList<Device>();
        private List<Food> foodList = new List<Food>();
        private List<Fish> livestockList = new List<Fish>();
        private List<Parameter> parameterList = new List<Parameter>();
        private double
            length,
            width,
            depth,
            volume,
            stockingLevel,
            waterTemp,
            pH;

        public List<Fish> LivestockList
        {
            get => livestockList;
            set => livestockList = value;
        }

        public List<Parameter> ParameterList
        {
            get => parameterList;
            set => parameterList = value;
        }

        public List<Food> FoodList
        {
            get => foodList;
            set => foodList = value;
        }

        public BindingList<Device> DeviceList
        {
            get => deviceList;
            set => deviceList = value;
        }

        internal Maintenance Maintenance
        {
            get => maintenance;
            set => maintenance = value;
        }

        internal MetaData MetaData
        {
            get => metaData;
            set => metaData = value;
        }

        public string PondTime
        {
            get => localPondTime;
            set => localPondTime = value;
        }

        public string AmFeedTime
        {
            get => morningFeedTime;
            set => morningFeedTime = value;
        }

        public string PmFeedTime
        {
            get => eveningFeedTime;
            set => eveningFeedTime = value;
        }

        public bool AmFeedDone
        {
            get => morningFeedComplete;
            set => morningFeedComplete = value;
        }

        public bool PmFeedDone
        {
            get => eveningFeedComplete;
            set => eveningFeedComplete = value;
        }

        public int FeedDuration
        {
            get => feedDuration;
            set => feedDuration = value;
        }

        public string Ip
        {
            get => IPAddress;
            set => IPAddress = value;
        }

        public int Port
        {
            get => port;
            set => port = value;
        }

        public double Length
        {
            get => length;
            set => length = value;
        }

        public double Width
        {
            get => width;
            set => width = value;
        }

        public double Depth
        {
            get => depth;
            set => depth = value;
        }

        public double Volume
        {
            get => volume;
            set => volume = value;
        }

        public double WaterTemp
        {
            get => waterTemp;
            set => waterTemp = value;
        }

        public double Ph
        {
            get => pH;
            set => pH = value;
        }

        public bool AdditionalFeed
        {
            get => additionalFeed; 
            set => additionalFeed = value;
        }

        public double StockingLevel
        {
            get
            {
                if (livestockList.Count == 0 || volume == 0) return 0;

                double totalFishLength = 0;
                double maxFishLength = (2.5 / 50) * volume;

                foreach (Fish f in livestockList) totalFishLength += f.Bioload;

                return stockingLevel = (totalFishLength / maxFishLength) * 100;
            }
        }

        public void AddDevice(Device newDevice)
        {
            deviceList.Add(newDevice);
        }

        public void AddFish(Fish newFish)
        {
            livestockList.Add(newFish);
        }

        public void RemoveFish(int index)
        {
            livestockList.RemoveAt(index);
        }

        public void AddFood(Food newFood)
        {
            foodList.Add(newFood);
        }

        public void RemoveFood(int index)
        {
            foodList.RemoveAt(index);
        }

        public void AddParameter(Parameter newRecord)
        {
            parameterList.Add(newRecord);
        }

        public void RemoveParameter(int index)
        {
            parameterList.RemoveAt(index);
        }

        public string GenerateDeviceCommand()
        {
	        var now = DateTime.Now;
	        var status = "";

            /*
             * Finds currently on devices
             */

            foreach (var d in deviceList)
            {
                if (status.Length == 4) break;

                if (d.Scheduled)
                {
                    var startTime = d.StartTime[0] + ":" + d.StartTime[1];
                    var endTime = d.EndTime[0] + ":" + d.EndTime[1];

                    var startTimeDateTime = DateTime.ParseExact(startTime, "HH:mm", CultureInfo.InvariantCulture);
                    var endTimeDateTime = DateTime.ParseExact(endTime, "HH:mm", CultureInfo.InvariantCulture);


                    if (startTimeDateTime < now && endTimeDateTime > now) status += 1;
                    else status += 0;
                }
                else if (!d.Scheduled)
                {
                    if (d.CurrentState) status += 1;
                    else status += 0;
                }
            }

            /*
             * Ensures the string has five digits
             */

	        while (status.Length < 4) status += "0";
	        return status;
        }
    }
}