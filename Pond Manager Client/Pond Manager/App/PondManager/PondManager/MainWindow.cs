﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using PondManager.Properties;
using Timer = System.Timers.Timer;

namespace PondManager
{
    public partial class MainWindow : Form
    {
        private readonly Timer controllerPollingTimer = new Timer();
        private readonly string currentDirectory;
        private readonly DataTable fishTable = new DataTable();
        private readonly DataTable foodTable = new DataTable();
        private readonly DataTable parameterTable = new DataTable();
        private readonly Timer UIRedrawTimer = new Timer();
        private bool editing, connected;
        private int currentlyEditingIndex;
        private Pond fishPond = new Pond();
        private string foodType, foodSize;

        public MainWindow(string workingDirectory)
        {
            /*
             * Used for saving and loading
             */
            currentDirectory = workingDirectory;

            /*
             * Generate any pond metadata
             */
            InitializeComponent();
            InitialisePond();
            InitialiseTables();

            /*
             * Setup UI
             */
            DrawOverviewTab();
            DrawPondTab();
            DrawFishTab();
            DrawParametersTab();
            DrawFoodTab();
            DrawPowerTab();
        }

        public void InitialisePond()
        {
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            LoadPond(currentDirectory);
            ipOverviewTB.Text = fishPond.Ip;
            portOverviewTB.Text = fishPond.Port.ToString();
            UIRedrawTimer.Interval = 2000;
            UIRedrawTimer.Elapsed += UiRedrawTimer_Elapsed;
            UIRedrawTimer.Start();
        }

        public void InitialiseTables()
        {
            CreateFishTable();
            CreateParameterTable();
            CreateFoodTable();
        }

        public void DrawOverviewTab()
        {
            PopulateParametersGb();
            PopulateRunningDevicesGb();
            PopulateSummaryGb();
        }

        public void DrawPondTab()
        {
            PopulateFishTable();
            PopulateDimensionsGb();
            PopulateTechnicalInfoGb();
            PopulateMetaDataGb();
            PopulateFeedInfoGb();
        }

        public void DrawFishTab()
        {
            removeFishBT.Enabled = fishPond.LivestockList.Count != 0;
            PopulateFishComboBox();
            PopulateFishTable();
        }

        public void DrawParametersTab()
        {
            csvWaterChemistryBT.Enabled = fishPond.ParameterList.Count != 0;
            removeRecordParametersBT.Enabled = fishPond.ParameterList.Count != 0;
            PopulateParametersTable();
        }

        public void DrawFoodTab()
        {
            removeFoodBT.Enabled = fishPond.FoodList.Count != 0;
            PopulateFoodTable();
        }

        public void DrawPowerTab()
        {
            removeDeviceBT.Enabled = fishPond.DeviceList.Count != 0;
            FormatDeviceFields();
            PopulateDeviceCb();
        }

        public void SavePond(string workingDirectory)
        {
            try
            {
                using (Stream stream = File.Open(workingDirectory, FileMode.Create))
                {
                    var binaryFormat = new BinaryFormatter();
                    binaryFormat.Serialize(stream, fishPond);
                }
            }
            catch
            {
                MessageBox.Show("Could not save file...");
            }
        }

        public void LoadPond(string workingDirectory)
        {
            if (!Fish.PopulateIndexList()) MessageBox.Show("Formatting error in FishIndex file...");

            try
            {
                using (Stream stream = File.Open(workingDirectory, FileMode.Open))
                {
                    var binaryFormat = new BinaryFormatter();
                    fishPond = (Pond) binaryFormat.Deserialize(stream);
                }
            }
            catch
            {
                SavePond(workingDirectory);
            }
        }

        public void PopulateParametersGb()
        {
            /*
             * Display a summary of most recent water test
             */

            tempOverviewLB.Text = "Temp: " + Math.Round(fishPond.WaterTemp, 1);
            pHOverviewLB.Text = "pH: " + fishPond.Ph;

            if (fishPond.WaterTemp < 5)
            {
                tempOverviewPB.Image = Resources.tempCOLD;
                tempOverviewLB.ForeColor = Color.Blue;
            }
            else if (fishPond.WaterTemp > 30)
            {
                tempOverviewPB.Image = Resources.tempHOT;
                tempOverviewLB.ForeColor = Color.Red;
            }
            else
            {
                tempOverviewPB.Image = Resources.temp;
                tempOverviewLB.ForeColor = Color.Black;
            }

            if (fishPond.Ph < 6.5 || fishPond.Ph > 8.5)
                pHOverviewLB.ForeColor = Color.Red;
            else
                pHOverviewLB.ForeColor = Color.Black;

            if (fishPond.ParameterList.Count != 0)
            {
                var np = fishPond.ParameterList[fishPond.ParameterList.Count - 1];

                ammoniaOverviewLB.Text = "Ammonia: " + np.Ammonia;
                ammoniaOverviewLB.ForeColor = np.Ammonia > 0 ? Color.Red : Color.Black;
                nitriteOverviewLB.Text = "Nitrite: " + np.Nitrite;
                nitrateOverviewLB.Text = "Nitrate: " + np.Nitrate;

                if (np.Nitrite > 0 && np.Nitrite < 0.25)
                    nitriteOverviewLB.ForeColor = Color.Orange;
                else if (np.Nitrite > 0.25)
                    nitriteOverviewLB.ForeColor = Color.Red;
                else
                    nitriteOverviewLB.ForeColor = Color.Black;

                if (np.Nitrate > 50 && np.Nitrate < 80)
                    nitrateOverviewLB.ForeColor = Color.Orange;
                else if (np.Nitrate > 80)
                    nitrateOverviewLB.ForeColor = Color.Red;
                else
                    nitrateOverviewLB.ForeColor = Color.Black;
            }
            else
            {
                ammoniaOverviewLB.Text = nitriteOverviewLB.Text = nitrateOverviewLB.Text = "No water tests logged";
                ammoniaOverviewLB.ForeColor = nitriteOverviewLB.ForeColor = nitrateOverviewLB.ForeColor = Color.Black;
            }
        }

        public void PopulateFishTable()
        {
            fishTable.Clear();
            foreach (var f in fishPond.LivestockList)
                fishTable.Rows.Add(f.Species, f.Size, f.Gender, f.Age, f.Comments);
        }

        public void PopulateFoodTable()
        {
            foodTable.Clear();
            foreach (var f in fishPond.FoodList) foodTable.Rows.Add(f.Name, f.Size, f.Type, f.Comments);
        }

        public void PopulateParametersTable()
        {
            parameterTable.Clear();

            foreach (var p in fishPond.ParameterList)
                parameterTable.Rows.Add(p.Date.ToShortDateString(), p.Temp.ToString(), p.Ph.ToString(),
                    p.Ammonia.ToString(), p.Nitrite.ToString(), p.Nitrate.ToString(),
                    p.Gh.ToString(), p.Kh.ToString());
        }

        public void CreateFishTable()
        {
            fishTable.Clear();
            fishTable.Columns.Add("Species", typeof(string));
            fishTable.Columns.Add("Size", typeof(string));
            fishTable.Columns.Add("Gender", typeof(string));
            fishTable.Columns.Add("Age", typeof(int));
            fishTable.Columns.Add("Comments", typeof(string));
            tableFishDGV.DataSource = fishTable;
        }

        public void CreateFoodTable()
        {
            foodTable.Clear();
            foodTable.Columns.Add("Name", typeof(string));
            foodTable.Columns.Add("Size", typeof(string));
            foodTable.Columns.Add("Type", typeof(string));
            foodTable.Columns.Add("Comments", typeof(string));
            tableFoodDGV.DataSource = foodTable;
        }

        public void CreateParameterTable()
        {
            parameterTable.Clear();
            parameterTable.Columns.Add("Date", typeof(string));
            parameterTable.Columns.Add("Temperature", typeof(string));
            parameterTable.Columns.Add("pH", typeof(string));
            parameterTable.Columns.Add("Ammonia", typeof(string));
            parameterTable.Columns.Add("Nitrite", typeof(string));
            parameterTable.Columns.Add("Nitrate", typeof(string));
            parameterTable.Columns.Add("GH", typeof(string));
            parameterTable.Columns.Add("KH", typeof(string));
            tableParametersDGV.DataSource = parameterTable;
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            currentDevicesLB.DataSource = fishPond.DeviceList;
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            SavePond(currentDirectory);
            UIRedrawTimer.Stop();
            controllerPollingTimer.Stop();
        }

        private void AddFishBT_Click(object sender, EventArgs e)
        {
            try
            {
                var size = double.Parse(sizeFishTB.Text);
                var gender = "Unknown";
                var comments = commentsFishTB.Text;
                var age = int.Parse(ageFishTB.Text);

                if (maleFishRB.Checked) gender = "Male";
                else if (femaleFishRB.Checked) gender = "Female";

                fishPond.AddFish(new Fish(fishCB.Text, size, gender, age, comments));
                PopulateFishTable();
                removeFishBT.Enabled = true;

                maleFishRB.Checked = femaleFishRB.Checked = unknownFishRB.Checked = false;
                sizeFishTB.Text = "";
                ageFishTB.Text = "";
                commentsFishTB.Text = "";
            }
            catch
            {
                MessageBox.Show("Please enter a valid fish");
            }

        }

        private void AddFoodBT_Click(object sender, EventArgs e)
        {
            fishPond.AddFood(new Food(nameFoodTB.Text, foodSize, foodType, commentsFoodTB.Text));
            PopulateFoodTable();
            removeFoodBT.Enabled = true;

            nameFoodTB.Text = "";
            commentsFoodTB.Text = "";
            flakeFoodRB.Checked = pelletFoodRB.Checked = sticksFoodRB.Checked = false;
            wheatgermFoodRB.Checked = stapleFoodRB.Checked = proteinFoodRB.Checked = false;
        }

        private void AddRecord_Click(object sender, EventArgs e)
        {
            try
            {
                fishPond.WaterTemp = double.Parse(temperatureParametersTB.Text);
                fishPond.Ph = double.Parse(phParametersTB.Text);
                fishPond.AddParameter(new Parameter(
                    DateTime.Parse(dateParametersDTP.Text),
                    double.Parse(temperatureParametersTB.Text),
                    double.Parse(phParametersTB.Text),
                    double.Parse(ammoniaParametersTB.Text),
                    double.Parse(nitriteParametersTB.Text),
                    double.Parse(nitrateParametersTB.Text),
                    double.Parse(ghParametersTB.Text),
                    double.Parse(khParametersTB.Text)
                ));

                PopulateParametersTable();
                removeRecordParametersBT.Enabled = true;
                csvWaterChemistryBT.Enabled = true;

                temperatureParametersTB.Text = "";
                temperatureParametersTB.Text = "";
                phParametersTB.Text = "";
                ammoniaParametersTB.Text = "";
                nitriteParametersTB.Text = "";
                nitrateParametersTB.Text = "";
                ghParametersTB.Text = "";
                khParametersTB.Text = "";
            }
            catch
            {
                MessageBox.Show("Please enter a valid record");
            }

        }

        private void ParameterGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            /*
             * Apply conditional formatting to parameter grid
             */

            double value;

            switch (tableParametersDGV.Columns[e.ColumnIndex].Name)
            {
                case "Temperature":
                    value = double.Parse(e.Value.ToString());
                    if (value < 5) e.CellStyle.BackColor = Color.LightBlue;
                    else if (value > 20) e.CellStyle.BackColor = Color.Pink;
                    break;

                case "pH":
                    value = double.Parse(e.Value.ToString());
                    if (value < 6.5 || value > 8.5) e.CellStyle.BackColor = Color.Pink;
                    break;

                case "Ammonia":
                    value = double.Parse(e.Value.ToString());
                    if (value > 0) e.CellStyle.BackColor = Color.Pink;
                    break;

                case "Nitrite":
                    value = double.Parse(e.Value.ToString());
                    if (value > 0 && value < 0.25) e.CellStyle.BackColor = Color.LightYellow;
                    else if (value > 0.25) e.CellStyle.BackColor = Color.Pink;
                    break;

                case "Nitrate":
                    value = double.Parse(e.Value.ToString());
                    if (value > 50 && value < 80) e.CellStyle.BackColor = Color.LightYellow;
                    else if (value > 80) e.CellStyle.BackColor = Color.Pink;
                    break;

                case "GH":
                    break;

                case "KH":
                    break;
            }
        }

        private void PopulateMetaDataGb()
        {
            typePondCB.Items.Clear();
            string[] pondTypes = {"Mixed", "Nature", "Goldfish", "Koi"};

            for (var i = 0; i < pondTypes.Length; i++)
            {
                typePondCB.Items.Add(pondTypes[i]);
                if (pondTypes[i] == fishPond.MetaData.PondType) typePondCB.SelectedIndex = i;
            }

            try
            {
                typePondCB.Text = fishPond.MetaData.PondType;
                plantedPondChB.Checked = fishPond.MetaData.PlantedPond;
                highSunPondChB.Checked = fishPond.MetaData.HighExposurePond;
                breedingPondChB.Checked = fishPond.MetaData.BreedingPond;
                growthPondChB.Checked = fishPond.MetaData.GrowthPond;
            }
            catch
            {
                MessageBox.Show("Error in save file, please delete and try again");
            }
        }

        private void PopulateFeedInfoGb()
        {
            feedNowPondBT.Enabled = connected;

            var morningFeed = fishPond.AmFeedTime.Split(':');
            var eveningFeed = fishPond.PmFeedTime.Split(':');

            mornFeedHrPondTB.Text = morningFeed[0];
            mornFeedMinPondTB.Text = morningFeed[1];

            eveFeedHrPondTB.Text = eveningFeed[0];
            eveFeedMinPondTB.Text = eveningFeed[1];

            var feedDuration = Math.Round((float) fishPond.FeedDuration / 1000, 1);
            correctionFactorPondLB.Text = "Feed duration (" + feedDuration + "s)";
            feedTimePondTrB.Value = fishPond.FeedDuration;
            mornFeedDonePondChB.Checked = fishPond.AmFeedDone;
            eveFeedDonePondChB.Checked = fishPond.PmFeedDone;
        }

        private void SectionTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
             * Update UI when tab changes
             */

            switch (navigationTC.SelectedIndex)
            {
                case 0:
                    UIRedrawTimer.Start();
                    DrawOverviewTab();
                    break;

                case 1:
                    UIRedrawTimer.Stop();
                    DrawPondTab();
                    break;

                case 2:
                    UIRedrawTimer.Stop();
                    DrawFishTab();
                    break;

                case 3:
                    UIRedrawTimer.Stop();
                    DrawParametersTab();
                    break;

                case 4:
                    UIRedrawTimer.Stop();
                    DrawFoodTab();
                    break;

                case 5:
                    UIRedrawTimer.Stop();
                    DrawPowerTab();
                    break;

                default:
                    UIRedrawTimer.Stop();
                    break;
            }
        }

        private void UiRedrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            /*
             * Redraw overview tab
             */

            if (InvokeRequired)
            {
                MethodInvoker methodInvoker = DrawOverviewTab;
                Invoke(methodInvoker);
            }
            else
            {
                DrawOverviewTab();
            }
        }

        private void FormatDeviceFields()
        {
            offDeviceRB.Enabled = true;
            onDeviceRB.Enabled = true;
            startHrDeviceTB.Enabled = false;
            startMinDeviceTB.Enabled = false;
            finishMinDeviceTB.Enabled = false;
            finishHrDeviceTB.Enabled = false;
        }

        private void PopulateDimensionsGb()
        {
            lengthPondTB.Text = fishPond.Length.ToString();
            widthPondTB.Text = fishPond.Width.ToString();
            depthPondTB.Text = fishPond.Depth.ToString();
        }

        private void PopulateTechnicalInfoGb()
        {
            /*
             * Work out some technical specifications for the pond
             */
            var flow = fishPond.Volume / 1.5;
            double correctionFactor = 1;
            var minUv = Math.Round(fishPond.Volume * 0.0018);
            var linerWidth = fishPond.Width + (fishPond.Depth * 2) + 1;
            var linerLength = fishPond.Length + (fishPond.Depth * 2) + 1;
            if (plantedPondChB.Checked) correctionFactor -= 0.25;
            if (highSunPondChB.Checked) correctionFactor += 0.5;
            if (breedingPondChB.Checked) correctionFactor += 0.5;
            if (growthPondChB.Checked) correctionFactor += 0.2;
            if (fishPond.MetaData.PondType == "Nature") correctionFactor -= 0.25;
            if (fishPond.MetaData.PondType == "Koi") correctionFactor += 0.5;
            if (fishPond.MetaData.PondType == "Goldfish") correctionFactor += 0.25;

            /*
             * Show the user the information
             */

            volumeOverviewPondLB.Text = "Current pond volume is " + fishPond.Volume + " litres";
            turnoverRatePondLB.Text = "Recommended turnover rate " + Math.Round((flow * correctionFactor),0) + " lph";
            uvRequiredPondLB.Text = "Minimum UV bulb strength is " + minUv + " watts";
            linerSizePondLB.Text = "Required liner size for this pond will be " + linerLength + " metres x " +
                                   linerWidth + " metres ";

            /*
             * Recommend an appropriate food
             */

            if (fishPond.WaterTemp >= 5 && fishPond.WaterTemp <= 10)
                recFoodPondLB.Text = "Recommended food for this water temperature is Wheatgerm";
            else if (fishPond.WaterTemp > 10 && fishPond.WaterTemp <= 18)
                recFoodPondLB.Text = "Recommended food for this water temperature is Staple";
            else if (fishPond.WaterTemp > 18)
                recFoodPondLB.Text = "Recommended food for this water temperature is High Protein Growth";
            else recFoodPondLB.Text = "Do not feed, water temperature too cold";

            /*
             * Recommend appropriate fish
             */
            
            if (fishPond.Volume == 0)
                fishSuitablePondLB.Text = "Please enter your pond dimensions to get fish recommendations";
            else if (fishPond.Volume > 0 && fishPond.Volume < 200)
                fishSuitablePondLB.Text = "Pond is too small for fish";
            else if (fishPond.Volume >= 200 && fishPond.Volume < 2500)
                fishSuitablePondLB.Text = "Suitable fish types are Goldfish, Comet, Shubunkin";
            else if (fishPond.Volume >= 2500 && fishPond.Volume < 6000)
                fishSuitablePondLB.Text = "Suitable fish types are Orfe, Tench, Goldfish, Shubunkin, Comet";
            else if (fishPond.Volume >= 6000 && fishPond.Volume < 10000)
                fishSuitablePondLB.Text = "Suitable fish types are Carp, Orfe, Tench, Goldfish, Shubunkin, Comet";
            else if (fishPond.Volume >= 6800 && fishPond.Volume < 10000 && fishPond.Depth >= 1.3)
                fishSuitablePondLB.Text = "Suitable fish types are Koi, Carp, Orfe, Tench, Goldfish, Shubunkin, Comet";
            else if (fishPond.Volume >= 10000 && fishPond.Depth >= 1.3)
                fishSuitablePondLB.Text = "Suitable fish types are Sturgeon, Koi, Carp, Orfe, Tench, Goldfish, Shubunkin, Comet";
            else
                fishSuitablePondLB.Text = "Suitable fish types are Orfe, Tench, Goldfish, Shubunkin, Comet";

        }

        private void PopulateSummaryGb()
        {
            /*
             * To work out how many fish you can accommodate, simply multiply the pond’s average length by its
             * average width to calculate the surface area and then apply the general rule of 1 inch of fish per square foot – or 25cm of fish for each square metre. 
             */


            /*
             * Generate stocking information
             */

            var stockingLevel = Math.Round(fishPond.StockingLevel,1);
            stockingOverviewLB.ForeColor =stockingLevel > 89 ? Color.Red : Color.Black;
            stockingOverviewLB.Text ="This pond is currently at " + stockingLevel + "% stocking";

            /*
             * Display maintenance reminders
             */

            if (fishPond.Maintenance.WaterChange < new DateTime(10))
                waterChangeOverviewLB.Text = "Water change reminders have not been setup yet...";
            else
                waterChangeOverviewLB.Text = "Water change due on " + fishPond.Maintenance.WaterChange.ToString("d");

            if (fishPond.Maintenance.FilterClean < new DateTime(10))
                filterCleanOverviewLB.Text = "Filter clean reminders have not been setup yet...";
            else
                filterCleanOverviewLB.Text =
                    "Filter clean required on " + fishPond.Maintenance.FilterClean.ToString("d");

            if (fishPond.Maintenance.PumpClean < new DateTime(10))
                pumpCleanOverviewLB.Text = "Pump clean reminders have not been setup yet...";
            else
                pumpCleanOverviewLB.Text = "Pump clean required on " + fishPond.Maintenance.PumpClean.ToString("d");

            if (fishPond.Maintenance.UvBulb < new DateTime(10))
                uvBulbOverviewLB.Text = "UV bulb reminders have not been setup yet...";
            else
                uvBulbOverviewLB.Text = "UV bulb replacement required on " + fishPond.Maintenance.UvBulb.ToString("d");

            if (fishPond.Maintenance.WaterTest < new DateTime(10))
                waterTestOverviewLB.Text = "Water test reminders have not been setup yet...";
            else
                waterTestOverviewLB.Text = "Water test due on " + fishPond.Maintenance.WaterTest.ToString("d");
        }

        public bool ExportCSV()
        {
            /*
             * Output the contents of the parameter list to a file
             */

            var dir = Directory.GetCurrentDirectory() + "\\Parameters.csv";

            using (var sw = new StreamWriter(dir))
            {
                if (fishPond.ParameterList.Count == 0) return false;
                sw.WriteLine("Temperature,pH,Ammonia,Nitrite,Nitrate,GH,KH");

                foreach (Parameter p in fishPond.ParameterList)
                {
                    var temp = p.Temp;
                    var pH = p.Ph;
                    var ammonia = p.Ammonia;
                    var nitrite = p.Nitrite;
                    var nitrate = p.Nitrate;
                    var gh = p.Gh;
                    var kh = p.Kh;
                    var line = $"{temp},{pH},{ammonia},{nitrite},{nitrate},{gh},{kh}";
                    sw.WriteLine(line);
                    sw.Flush();
                }
            }
            return true;
        }

        private void PopulateRunningDevicesGb()
        {
            /*
             * Show the devices currently powered by the controller
             */

            ClearDeviceFields();
            var onDevices = new List<Device>();
            var labels = new List<Label>
            {
                device1OverviewLB,
                device2OverviewLB,
                device3OverviewLB,
                device4OverviewLB,
            };

            /*
             * Iterate through device list, add any ON devices to new list
             */

            if (fishPond.DeviceList.Count != 0)
            {
                int count = 0;
                foreach (Device d in fishPond.DeviceList)
                {
                    if (count >= 4) break;

                    var currentTime = DateTime.Now;

                    if (d.Scheduled)
                    {
                        var startTimeStr = d.StartTime[0] + ":" +
                                           d.StartTime[1];
                        var endTimeStr = d.EndTime[0] + ":" + d.EndTime[1];
                        var startTime = DateTime.ParseExact(startTimeStr, "HH:mm", CultureInfo.InvariantCulture);
                        var endTime = DateTime.ParseExact(endTimeStr, "HH:mm", CultureInfo.InvariantCulture);
                        if (startTime < currentTime && endTime > currentTime) onDevices.Add(d);
                    }
                    else
                    {
                        if (d.CurrentState) onDevices.Add(d);
                    }
                }

            }

            /*
             * Add ON devices to group box on overview screen
             */

            if (onDevices.Count != 0)
            {
                var count = 0;
                foreach (var l in labels)
                {
                    if (count >= onDevices.Count) continue;

                    try
                    {
                        var pb = new PictureBox();
                        var pbLoc = l.Location;
                        l.Text = onDevices[count].DeviceName;
                        pbLoc.X -= 40;
                        pbLoc.Y -= 10;
                        pb.Location = pbLoc;

                        switch (onDevices[count].DeviceType)
                        {
                            case DeviceType.Filter:
                                pb.Image = Resources.filter;
                                pb.Height = Resources.filter.Height;
                                pb.Width = Resources.filter.Width;
                                break;
                            case DeviceType.Pump:
                                pb.Image = Resources.pump;
                                pb.Height = Resources.pump.Height;
                                pb.Width = Resources.pump.Width;
                                break;
                            case DeviceType.Uv:
                                pb.Image = Resources.uv;
                                pb.Height = Resources.pump.Height;
                                pb.Width = Resources.pump.Width;
                                break;
                            case DeviceType.Skimmer:
                                pb.Image = Resources.skim;
                                pb.Height = Resources.pump.Height;
                                pb.Width = Resources.pump.Width;
                                break;
                            case DeviceType.Lights:
                                pb.Image = Resources.lights;
                                pb.Height = Resources.pump.Height;
                                pb.Width = Resources.pump.Width;
                                break;
                            default:
                                pb.Image = Resources.other;
                                pb.Height = Resources.pump.Height;
                                pb.Width = Resources.pump.Width;
                                break;
                        }

                        devicesOverviewGB.Controls.Add(pb);
                    }
                    catch
                    {
                    }
                    finally
                    {
                        count++;
                    }
                }
            }

            if (onDevices.Count == 0)
            {
                device1OverviewLB.Text = "No currently active devices";
                var pb = new PictureBox();
                var pbLoc = device1OverviewLB.Location;
                pbLoc.X -= 40;
                pbLoc.Y -= 10;
                pb.Location = pbLoc;
                pb.Image = Resources.nodev;
                pb.Height = Resources.filter.Height;
                pb.Width = Resources.filter.Width;
                devicesOverviewGB.Controls.Add(pb);
            }
        }

        private void ConnectBT_Click(object sender, EventArgs e)
        {
            /*
             * Attempt to connect to the controller
             */

            try
            {
                Controller.IP = ipOverviewTB.Text;
                Controller.Port = int.Parse(portOverviewTB.Text);

                if (Controller.Available())
                {
                    ipOverviewTB.Enabled = portOverviewTB.Enabled = connectOverviewBT.Enabled = false;
                    disconnectOverviewBT.Enabled = true;
                    fishPond.Ip = ipOverviewTB.Text;
                    fishPond.Port = int.Parse(portOverviewTB.Text);
                    controllerPollingTimer.Interval = 5000;
                    controllerPollingTimer.Enabled = true;
                    controllerPollingTimer.Elapsed += ControllerPollingTimer_Elapsed;
                }
                else
                {
                    connected = false;
                    ipOverviewTB.Enabled = portOverviewTB.Enabled = portOverviewTB.Enabled = true;
                    connStatusOverviewLB.ForeColor = Color.Red;
                    connectOverviewBT.Enabled = false;
                    disconnectOverviewBT.Enabled = true;
                    connStatusOverviewLB.Text = "Current Status: Fault";
                }
            }
            catch
            {
                MessageBox.Show("Please enter a valid IP address and port");
            }
        }

        private void DisconnectBT_Click(object sender, EventArgs e)
        {
            /*
             * Disconnect from controller
             */

            connected = false;
            controllerPollingTimer.Stop();
            controllerPollingTimer.Enabled = false;
            disconnectOverviewBT.Enabled = false;
            connStatusOverviewLB.ForeColor = Color.Black;
            connStatusOverviewLB.Text = "Current Status: Disconnected";
            timeOverviewLB.Text = "Local time: Unavailable";
            connStatusOverviewLB.ForeColor = Color.Black;
            ipOverviewTB.Enabled = portOverviewTB.Enabled = connectOverviewBT.Enabled = disconnectOverviewBT.Enabled = true;
            PopulateParametersGb();
        }

        private void ControllerPollingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            /*
             * Connect status update delegates
             */

            var UIconnected = new MethodInvoker(
                delegate
                {
                    connStatusOverviewLB.ForeColor = Color.Green;
                    connStatusOverviewLB.Text = "Current Status: Connected";
                    timeOverviewLB.Text = "Local time: " + fishPond.PondTime;
                    feedNowPondBT.Enabled = true;
                }
            );

            var UIdisconnected = new MethodInvoker(
                delegate
                {
                    connStatusOverviewLB.Text = "Current Status: Disconnected";
                    ipOverviewTB.Enabled = true;
                    portOverviewTB.Enabled = true;
                    connStatusOverviewLB.ForeColor = Color.Black;
                    feedNowPondBT.Enabled = false;
                });

            var UIrefreshCheckboxes = new MethodInvoker(
                delegate
                {
                    mornFeedDonePondChB.Checked = fishPond.AmFeedDone;
                    eveFeedDonePondChB.Checked = fishPond.PmFeedDone;
                });

            /*
             * Send controller requests
             */

            try
            {
                if (!Controller.Available()) throw new Exception();

                connected = true;
                fishPond.PondTime = Controller.LocalTime();
                fishPond.WaterTemp = Controller.WaterTemperature();
                fishPond.Ph = Controller.pH();
                Controller.UpdateDevices(fishPond.GenerateDeviceCommand());

                Invoke(UIconnected);

                var morningFeed = DateTime.ParseExact(fishPond.AmFeedTime, "HH:mm", CultureInfo.InvariantCulture);
                var eveningFeed = DateTime.ParseExact(fishPond.PmFeedTime, "HH:mm", CultureInfo.InvariantCulture);
                var pondTime = DateTime.ParseExact(fishPond.PondTime, "HH:mm", CultureInfo.InvariantCulture);

                if (fishPond.AdditionalFeed == true)
                {
                    fishPond.AdditionalFeed = false;
                    Controller.Feed(fishPond.FeedDuration);
                }

                if (morningFeed == pondTime && fishPond.AmFeedDone == false)
                    if (Controller.Feed(fishPond.FeedDuration))
                    {
                        fishPond.AmFeedDone = true;
                        fishPond.PmFeedDone = false;
                    }

                if (eveningFeed == pondTime && fishPond.PmFeedDone == false)
                    if (Controller.Feed(fishPond.FeedDuration))
                    {
                        fishPond.PmFeedDone = true;
                        fishPond.AmFeedDone = false;
                    }

                Invoke(UIrefreshCheckboxes);
            }
            catch
            {
                connected = false;
                Invoke(UIdisconnected);
            }
        }

        private void PopulateDeviceCb()
        {
            typeDeviceCB.Items.Clear();
            foreach (DeviceType device in Enum.GetValues(typeof(DeviceType)))
                typeDeviceCB.Items.Add(new DeviceComboBoxItem(device));
        }

        private void PopulateFishComboBox()
        {
            fishCB.Items.Clear();
            foreach (var entry in Fish.IndexList) fishCB.Items.Add(entry.Key);
        }

        private void SubmitDeviceBT_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(nameDeviceLB.Text) ||
                typeDeviceCB.SelectedIndex == -1)
            {
                MessageBox.Show("Please enter valid times");
            }

            var deviceState = onDeviceRB.Checked;
            DeviceType selectedDevice;

            switch (typeDeviceCB.SelectedIndex)
            {
                case 0:
                    selectedDevice = DeviceType.Filter;
                    break;
                case 1:
                    selectedDevice = DeviceType.Pump;
                    break;
                case 2:
                    selectedDevice = DeviceType.Uv;
                    break;
                case 3:
                    selectedDevice = DeviceType.Skimmer;
                    break;
                case 4:
                    selectedDevice = DeviceType.Lights;
                    break;
                default:
                    selectedDevice = DeviceType.Other;
                    break;
            }

            /*
             * If device is marked as scheduled, parse user input and store times
             */

            if (scheduledDeviceChB.Checked)
            {
                var count = 0;
                var isValid = false;
                var startTime = new[] {startHrDeviceTB.Text, startMinDeviceTB.Text};
                var endTime = new[] {finishHrDeviceTB.Text, finishMinDeviceTB.Text};

                try
                {
                    if (startTime[0] + startTime[1] != endTime[0] + endTime[1])
                        foreach (Control tb in scheduleDeviceGB.Controls)
                            if (tb is TextBox)
                            {
                                var number = int.Parse(tb.Text);

                                if (count == 2 || count == 3) //Hours text boxes
                                {
                                    if (number > 23 || number < 0) throw new Exception();
                                    isValid = true;
                                }
                                else if (count == 0 || count == 1) // Minutes text boxes
                                {
                                    if (number > 59 || number < 0) throw new Exception();
                                    isValid = true;
                                }

                                count++;
                            }
                }
                catch (Exception)
                {
                    isValid = false;
                }

                /*
                 * If the user input is correct, add a new device
                 */

                if (isValid)
                {
                    var newDevice = new Device(nameDeviceTB.Text, selectedDevice, scheduledDeviceChB.Checked,
                        deviceState, startTime, endTime);

                    if (editing)
                    {
                        fishPond.DeviceList[currentlyEditingIndex] = newDevice;
                        editing = false;
                        addDeviceBT.Text = "Add";
                    }
                    else
                    {
                        fishPond.AddDevice(newDevice);
                    }

                    currentDevicesLB.DataSource = null;
                    currentDevicesLB.DataSource = fishPond.DeviceList;
                    ClearDeviceFields();
                }
                else
                {
                    MessageBox.Show("Please enter valid schedule times");
                }
            }

            else
            {
                var blankTime = new[] {"00", "00"};
                var newDevice = new Device(nameDeviceTB.Text, selectedDevice, scheduledDeviceChB.Checked, deviceState,
                    blankTime, blankTime);

                if (editing)
                {
                    fishPond.DeviceList[currentlyEditingIndex] = newDevice;
                    editing = false;
                    addDeviceBT.Text = "Add";
                }
                else
                {
                    fishPond.AddDevice(newDevice);
                }

                currentDevicesLB.DataSource = null;
                currentDevicesLB.DataSource = fishPond.DeviceList;
                ClearDeviceFields();
            }
        }

        private void ClearDeviceFields()
        {
            nameDeviceTB.Text = startHrDeviceTB.Text = startMinDeviceTB.Text = finishHrDeviceTB.Text = finishMinDeviceTB.Text = "";
            offDeviceRB.Checked = onDeviceRB.Checked = false;
            scheduledDeviceChB.Checked = false;
            typeDeviceCB.SelectedItem = null;

            foreach (Control lb in devicesOverviewGB.Controls) lb.Text = "";
            foreach (Control pb in devicesOverviewGB.Controls)
                if (pb is PictureBox picture)
                    devicesOverviewGB.Controls.Remove(picture);
        }

        private void ScheduledCB_CheckedChanged(object sender, EventArgs e)
        {
            /*
             * Enable/disable scheduled controls as necessary
             */

            if (scheduledDeviceChB.Checked)
            {
                offDeviceRB.Enabled = false;
                onDeviceRB.Enabled = false;
                startHrDeviceTB.Enabled = true;
                startMinDeviceTB.Enabled = true;
                finishMinDeviceTB.Enabled = true;
                finishHrDeviceTB.Enabled = true;
            }
            else
            {
                offDeviceRB.Enabled = true;
                onDeviceRB.Enabled = true;
                startHrDeviceTB.Enabled = false;
                startMinDeviceTB.Enabled = false;
                finishMinDeviceTB.Enabled = false;
                finishHrDeviceTB.Enabled = false;
            }
        }

        private void RemoveDevicesBT_Click(object sender, EventArgs e)
        {
            if (fishPond.DeviceList.Count == 0 ||
                currentDevicesLB.SelectedIndex == -1) return;

            fishPond.DeviceList.RemoveAt(currentDevicesLB.SelectedIndex);
            currentDevicesLB.DataSource = fishPond.DeviceList;
        }

        private void RemoveFishBT_Click(object sender, EventArgs e)
        {
            if (fishPond.LivestockList.Count == 0 || 
                tableFishDGV.CurrentCell.RowIndex == -1) return;

            var index = tableFishDGV.CurrentCell.RowIndex;
            fishPond.RemoveFish(index);
            PopulateFishTable();
        }

        private void RemoveRecordBT_Click(object sender, EventArgs e)
        {
            if (fishPond.ParameterList.Count == 0 ||
                tableParametersDGV.CurrentCell.RowIndex == -1) return;

            var index = tableParametersDGV.CurrentCell.RowIndex;
            fishPond.RemoveParameter(index);
            PopulateParametersTable();
        }

        private void RemoveFoodBT_Click(object sender, EventArgs e)
        {
            if (fishPond.FoodList.Count == 0 || 
                tableFoodDGV.CurrentCell.RowIndex == -1) return;

            var index = tableFoodDGV.CurrentCell.RowIndex;
            fishPond.RemoveFood(index);
            PopulateFoodTable();
        }

        private void SetupLLB_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            /*
             * Update maintenance reminders information
             */

            using (var setup = new MaintenanceLogger())
            {
                var result = setup.ShowDialog();

                if (result == DialogResult.OK)
                {
                    if (fishPond.Maintenance.FilterClean < setup.Maintenance.FilterClean)
                        fishPond.Maintenance.FilterClean = setup.Maintenance.FilterClean;

                    if (fishPond.Maintenance.PumpClean < setup.Maintenance.PumpClean)
                        fishPond.Maintenance.PumpClean = setup.Maintenance.PumpClean;

                    if (fishPond.Maintenance.UvBulb < setup.Maintenance.UvBulb)
                        fishPond.Maintenance.UvBulb = setup.Maintenance.UvBulb;

                    if (fishPond.Maintenance.WaterChange < setup.Maintenance.WaterChange)
                        fishPond.Maintenance.WaterChange = setup.Maintenance.WaterChange;

                    if (fishPond.Maintenance.WaterTest < setup.Maintenance.WaterTest)
                        fishPond.Maintenance.WaterTest = setup.Maintenance.WaterTest;
                }

                PopulateSummaryGb();
            }
        }

        private void LoadDeviceBT_Click(object sender, EventArgs e)
        {
            /*
             * Populate all fields for editing after loading saved device
             */

            if (currentDevicesLB.SelectedIndex == -1) return;

            ClearDeviceFields();
            editing = true;
            addDeviceBT.Text = "Save";
            currentlyEditingIndex = currentDevicesLB.SelectedIndex;
            var currentDevice = fishPond.DeviceList[currentlyEditingIndex];
            nameDeviceTB.Text = currentDevice.DeviceName;

            switch (currentDevice.DeviceType)
            {
                case DeviceType.Filter:
                    typeDeviceCB.SelectedIndex = 0;
                    break;
                case DeviceType.Pump:
                    typeDeviceCB.SelectedIndex = 1;
                    break;
                case DeviceType.Uv:
                    typeDeviceCB.SelectedIndex = 2;
                    break;
                case DeviceType.Skimmer:
                    typeDeviceCB.SelectedIndex = 3;
                    break;
                case DeviceType.Lights:
                    typeDeviceCB.SelectedIndex = 4;
                    break;
                default:
                    typeDeviceCB.SelectedIndex = 5;
                    break;
            }

            if (currentDevice.Scheduled)
            {
                scheduledDeviceChB.Checked = true;
                offDeviceRB.Enabled = false;
                onDeviceRB.Enabled = false;
                startHrDeviceTB.Text = currentDevice.StartTime[0];
                startMinDeviceTB.Text = currentDevice.StartTime[1];
                finishHrDeviceTB.Text = currentDevice.EndTime[0];
                finishMinDeviceTB.Text = currentDevice.EndTime[1];

                var startTime = currentDevice.StartTime[0] + ":" + currentDevice.StartTime[1];
                var endTime = currentDevice.EndTime[0] + ":" + currentDevice.EndTime[1];

                var startTimeDateTime = DateTime.ParseExact(startTime, "HH:mm", CultureInfo.InvariantCulture);
                var endTimeDateTime = DateTime.ParseExact(endTime, "HH:mm", CultureInfo.InvariantCulture);
                var now = DateTime.Now;

                if (startTimeDateTime < now && endTimeDateTime > now)
                {
                    onDeviceRB.Checked = true;
                    offDeviceRB.Checked = false;
                }
                else
                {
                    onDeviceRB.Checked = false;
                    offDeviceRB.Checked = true;
                }
            }
            else
            {
                switch (currentDevice.CurrentState)
                {
                    case true:
                        onDeviceRB.Checked = true;
                        offDeviceRB.Checked = false;
                        break;
                    case false:
                        onDeviceRB.Checked = false;
                        offDeviceRB.Checked = true;
                        break;
                }
            }

            currentDevicesLB.ClearSelected();
        }

        private void FeedAmountSR_ValueChanged(object sender, EventArgs e)
        {
            var value = Math.Round((float) feedTimePondTrB.Value / 1000, 1);
            correctionFactorPondLB.Text = "Feed duration (" + value + "s)";
            fishPond.FeedDuration = feedTimePondTrB.Value;
        }

        private void NewDimensions_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            UpdateDimensions();
        }

        private void UpdateDimensions()
        {
            try
            {
                if (widthPondTB.Text == "0" ||
                    lengthPondTB.Text == "0" ||
                    depthPondTB.Text == "0") throw new Exception();

                fishPond.Length = double.Parse(lengthPondTB.Text);
                fishPond.Width = double.Parse(widthPondTB.Text);
                fishPond.Depth = double.Parse(depthPondTB.Text);
                fishPond.Volume = 1000 * (fishPond.Length * fishPond.Width * fishPond.Depth);
                PopulateTechnicalInfoGb();
            }
            catch
            {
                MessageBox.Show("Please enter valid dimensions");
            }
        }

        private void TypePondCB_SelectionChangeCommitted(object sender, EventArgs e) //here
        {
            fishPond.MetaData.PondType = typePondCB.Text;
            if (plantedPondChB.Checked) fishPond.MetaData.PlantedPond = true;
            if (highSunPondChB.Checked) fishPond.MetaData.HighExposurePond = true;
            if (breedingPondChB.Checked) fishPond.MetaData.BreedingPond = true;
            if (growthPondChB.Checked) fishPond.MetaData.GrowthPond = true;
            PopulateTechnicalInfoGb();
        }

        private void FeedTimeTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            UpdateFeedTime();
        }

        private void UpdateFeedTime()
        {
            try
            {
                /*
                 * Check the input for each field is two digits long (24hr)
                 */

                if (mornFeedHrPondTB.Text.Length != 2 ||
                    mornFeedMinPondTB.Text.Length != 2 ||
                    eveFeedHrPondTB.Text.Length != 2 ||
                    eveFeedMinPondTB.Text.Length != 2)
                    throw new Exception();

                var mornFeedHr = int.Parse(mornFeedHrPondTB.Text);
                var mornFeedMin = int.Parse(mornFeedMinPondTB.Text);
                var eveFeedHr = int.Parse(eveFeedHrPondTB.Text);
                var eveFeedMin = int.Parse(eveFeedMinPondTB.Text);

                /*
                 * Check the inputted values are valid times
                 */

                if (mornFeedHr > 23 ||
                    mornFeedMin > 59 ||
                    eveFeedHr > 23 ||
                    eveFeedMin > 59)
                    throw new Exception();

                /*
                 * Update the class members
                 */

                fishPond.AmFeedTime = mornFeedHrPondTB.Text + ":" + mornFeedMinPondTB.Text;
                fishPond.PmFeedTime = eveFeedHrPondTB.Text + ":" + eveFeedMinPondTB.Text;
            }
            catch
            {
                MessageBox.Show("Please enter a valid 24h time");
            }
        }

        private void feedNowPondBT_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you'd like to feed now?",
                "Feed Request", MessageBoxButtons.YesNo);

            if (result == DialogResult.Yes) fishPond.AdditionalFeed = true;
        }

        private void mornFeedDonePondChB_CheckedChanged(object sender, EventArgs e)
        {
            fishPond.AmFeedDone = eveFeedDonePondChB.Checked;
        }

        private void eveFeedDonePondChB_CheckedChanged(object sender, EventArgs e)
        {
            fishPond.PmFeedDone = eveFeedDonePondChB.Checked;
        }

        private void csvWaterChemistryBT_Click(object sender, EventArgs e)
        {
            MessageBox.Show( ExportCSV() ?
                "CSV file written successfully" : "CSV file writing failed");
        }

        private void setTimesPondBT_Click(object sender, EventArgs e)
        {
            UpdateFeedTime();
        }

        private void dimensionsPondBT_Click(object sender, EventArgs e)
        {
            UpdateDimensions();
        }

        private void FoodTypeBT_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control c in sizeFoodGB.Controls)
                if (c is RadioButton)
                {
                    var radio = c as RadioButton;
                    if (radio.Checked) foodSize = radio.Text;
                }

            foreach (Control c in typeFoodGB.Controls)
                if (c is RadioButton)
                {
                    var radio = c as RadioButton;
                    if (radio.Checked) foodType = radio.Text;
                }
        }
    }

    internal class DeviceComboBoxItem
    {
        private DeviceType Device { get; }

        public DeviceComboBoxItem(DeviceType device)
        {
            this.Device = device;
        }

        public override string ToString()
        {
            switch (Device)
            {
                case DeviceType.Filter:
                    return "Filter";
                case DeviceType.Pump:
                    return "Pump";
                case DeviceType.Uv:
                    return "UV Clarifier";
                case DeviceType.Skimmer:
                    return "Skimmer";
                case DeviceType.Lights:
                    return "Lights";
                default:
                    return "Other";
            }
        }
    }
}