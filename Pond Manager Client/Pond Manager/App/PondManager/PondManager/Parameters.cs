﻿using System;

namespace PondManager
{
    [Serializable]
    internal class Parameter
    {
        private DateTime date;
        private double
            temp,
            PH,
            ammonia,
            nitrite,
            nitrate,
            GH,
            KH;

        public Parameter(DateTime date, double temp, double pH, double ammonia, double nitrite, double nitrate,
            double gh, double kh)
        {
            this.temp = temp;
            this.date = date;
            PH = pH;
            this.ammonia = ammonia;
            this.nitrite = nitrite;
            this.nitrate = nitrate;
            GH = gh;
            KH = kh;
        }

        public DateTime Date
        {
            get => date;
            set => date = value;
        }

        public double Temp
        {
            get => temp;
            set => temp = value;
        }

        public double Ph
        {
            get => PH;
            set => PH = value;
        }

        public double Ammonia
        {
            get => ammonia;
            set => ammonia = value;
        }

        public double Nitrite
        {
            get => nitrite;
            set => nitrite = value;
        }

        public double Nitrate
        {
            get => nitrate;
            set => nitrate = value;
        }

        public double Gh
        {
            get => GH;
            set => GH = value;
        }

        public double Kh
        {
            get => KH;
            set => KH = value;
        }
    }
}