﻿using System;

namespace PondManager
{
    [Serializable]
    internal class Food
    {
        private string
            comments,
            name,
            size,
            type;

        public Food(string name, string size, string type, string comments)
        {
            this.name = name;
            this.size = size;
            this.type = type;
            this.comments = comments;
        }

        public string Name
        {
            get => name;
            set => name = value;
        }

        public string Size
        {
            get => size;
            set => size = value;
        }

        public string Type
        {
            get => type;
            set => type = value;
        }

        public string Comments
        {
            get => comments;
            set => comments = value;
        }
    }
}