﻿using System;

namespace PondManager
{
    internal enum DeviceType
    {
        Filter,
        Pump,
        Uv,
        Skimmer,
        Lights,
        Other
    }

    [Serializable]
    internal class Device
    {
        private bool currentState, scheduled;
        private string deviceName;
        private DeviceType deviceType;
        private string[] endTime;
        private string[] startTime;

        public Device(string deviceName, DeviceType type, bool isScheduled, bool currentState, string[] startTime, string[] endTime)
        {
            this.deviceName = deviceName;

            this.scheduled = isScheduled;
            this.currentState = currentState;

            this.startTime = startTime;
            this.endTime = endTime;

            deviceType = type;
        }

        public bool CurrentState { get => currentState; set => currentState = value; }

        public bool Scheduled { get => scheduled; set => scheduled = value; }

        public string DeviceName { get => deviceName; set => deviceName = value; }

        public DeviceType DeviceType { get => deviceType; set => deviceType = value; }

        public string[] StartTime { get => startTime; set => startTime = value; }

        public string[] EndTime { get => endTime; set => endTime = value; }

        public override string ToString() { return deviceName; }
    }
}
