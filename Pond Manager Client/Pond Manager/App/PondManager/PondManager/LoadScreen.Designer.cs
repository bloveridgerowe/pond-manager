﻿namespace PondManager
{
    partial class LoadPond
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadPond));
            this._loadBT = new System.Windows.Forms.Button();
            this.savesList = new System.Windows.Forms.ListBox();
            this._newBT = new System.Windows.Forms.Button();
            this._loadGB = new System.Windows.Forms.GroupBox();
            this._removeBT = new System.Windows.Forms.Button();
            this._newGB = new System.Windows.Forms.GroupBox();
            this.pondNameTb = new System.Windows.Forms.TextBox();
            this._loadGB.SuspendLayout();
            this._newGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // _loadBT
            // 
            this._loadBT.Location = new System.Drawing.Point(207, 44);
            this._loadBT.Name = "_loadBT";
            this._loadBT.Size = new System.Drawing.Size(75, 23);
            this._loadBT.TabIndex = 1;
            this._loadBT.Text = "Load";
            this._loadBT.UseVisualStyleBackColor = true;
            this._loadBT.Click += new System.EventHandler(this.LoadBT_Click);
            // 
            // savesList
            // 
            this.savesList.FormattingEnabled = true;
            this.savesList.Location = new System.Drawing.Point(12, 21);
            this.savesList.Name = "savesList";
            this.savesList.Size = new System.Drawing.Size(165, 95);
            this.savesList.TabIndex = 2;
            // 
            // _newBT
            // 
            this._newBT.Location = new System.Drawing.Point(208, 19);
            this._newBT.Name = "_newBT";
            this._newBT.Size = new System.Drawing.Size(75, 23);
            this._newBT.TabIndex = 3;
            this._newBT.Text = "New";
            this._newBT.UseVisualStyleBackColor = true;
            this._newBT.Click += new System.EventHandler(this.NewBT_Click);
            // 
            // _loadGB
            // 
            this._loadGB.Controls.Add(this._removeBT);
            this._loadGB.Controls.Add(this.savesList);
            this._loadGB.Controls.Add(this._loadBT);
            this._loadGB.Location = new System.Drawing.Point(12, 8);
            this._loadGB.Name = "_loadGB";
            this._loadGB.Size = new System.Drawing.Size(310, 128);
            this._loadGB.TabIndex = 4;
            this._loadGB.TabStop = false;
            this._loadGB.Text = "Load";
            // 
            // _removeBT
            // 
            this._removeBT.Location = new System.Drawing.Point(207, 73);
            this._removeBT.Name = "_removeBT";
            this._removeBT.Size = new System.Drawing.Size(75, 23);
            this._removeBT.TabIndex = 3;
            this._removeBT.Text = "Remove";
            this._removeBT.UseVisualStyleBackColor = true;
            this._removeBT.Click += new System.EventHandler(this.RemovePondBT_Click);
            // 
            // _newGB
            // 
            this._newGB.Controls.Add(this.pondNameTb);
            this._newGB.Controls.Add(this._newBT);
            this._newGB.Location = new System.Drawing.Point(11, 142);
            this._newGB.Name = "_newGB";
            this._newGB.Size = new System.Drawing.Size(310, 55);
            this._newGB.TabIndex = 5;
            this._newGB.TabStop = false;
            this._newGB.Text = "New";
            // 
            // pondNameTb
            // 
            this.pondNameTb.Location = new System.Drawing.Point(12, 21);
            this.pondNameTb.Name = "pondNameTb";
            this.pondNameTb.Size = new System.Drawing.Size(165, 20);
            this.pondNameTb.TabIndex = 4;
            // 
            // LoadPond
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 205);
            this.Controls.Add(this._newGB);
            this.Controls.Add(this._loadGB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "LoadPond";
            this.Text = "Pond Picker";
            this._loadGB.ResumeLayout(false);
            this._newGB.ResumeLayout(false);
            this._newGB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _loadBT;
        private System.Windows.Forms.ListBox savesList;
        private System.Windows.Forms.Button _newBT;
        private System.Windows.Forms.GroupBox _loadGB;
        private System.Windows.Forms.GroupBox _newGB;
        private System.Windows.Forms.TextBox pondNameTb;
        private System.Windows.Forms.Button _removeBT;
    }
}