﻿using System;

namespace PondManager
{
    [Serializable]
    internal class MetaData
    {
        private string mPondType;
        private bool
            mBreedingPond,
            mGrowthPond,
            mHighExposurePond,
            mPlantedPond;

        internal string PondType
        {
            get => mPondType;
            set => mPondType = value;
        }

        internal bool PlantedPond
        {
            get => mPlantedPond;
            set => mPlantedPond = value;
        }

        internal bool HighExposurePond
        {
            get => mHighExposurePond;
            set => mHighExposurePond = value;
        }

        internal bool BreedingPond
        {
            get => mBreedingPond;
            set => mBreedingPond = value;
        }

        internal bool GrowthPond
        {
            get => mGrowthPond;
            set => mGrowthPond = value;
        }
    }
}