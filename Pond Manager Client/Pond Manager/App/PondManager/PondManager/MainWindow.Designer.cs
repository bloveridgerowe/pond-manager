﻿namespace PondManager
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.navigationTC = new System.Windows.Forms.TabControl();
            this.overviewTP = new System.Windows.Forms.TabPage();
            this.maintenanceOverviewGB = new System.Windows.Forms.GroupBox();
            this.remindersOverviewLL = new System.Windows.Forms.LinkLabel();
            this.waterTestOverviewPB = new System.Windows.Forms.PictureBox();
            this.filterCleanOverviewPB = new System.Windows.Forms.PictureBox();
            this.pumpCleanOverviewPB = new System.Windows.Forms.PictureBox();
            this.uvBulbOverviewPB = new System.Windows.Forms.PictureBox();
            this.waterChangeOverviewPB = new System.Windows.Forms.PictureBox();
            this.stockingOverviewPB = new System.Windows.Forms.PictureBox();
            this.waterTestOverviewLB = new System.Windows.Forms.Label();
            this.filterCleanOverviewLB = new System.Windows.Forms.Label();
            this.pumpCleanOverviewLB = new System.Windows.Forms.Label();
            this.uvBulbOverviewLB = new System.Windows.Forms.Label();
            this.waterChangeOverviewLB = new System.Windows.Forms.Label();
            this.stockingOverviewLB = new System.Windows.Forms.Label();
            this.controllerOverviewGB = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.timeOverviewLB = new System.Windows.Forms.Label();
            this.connStatusOverviewLB = new System.Windows.Forms.Label();
            this.portOverviewLB = new System.Windows.Forms.Label();
            this.ipOverviewLB = new System.Windows.Forms.Label();
            this.portColonLB = new System.Windows.Forms.Label();
            this.portOverviewTB = new System.Windows.Forms.TextBox();
            this.ipOverviewTB = new System.Windows.Forms.TextBox();
            this.disconnectOverviewBT = new System.Windows.Forms.Button();
            this.connectOverviewBT = new System.Windows.Forms.Button();
            this.devicesOverviewGB = new System.Windows.Forms.GroupBox();
            this.device5OverviewLB = new System.Windows.Forms.Label();
            this.device3OverviewLB = new System.Windows.Forms.Label();
            this.device1OverviewLB = new System.Windows.Forms.Label();
            this.device2OverviewLB = new System.Windows.Forms.Label();
            this.device4OverviewLB = new System.Windows.Forms.Label();
            this.parametersOverviewGB = new System.Windows.Forms.GroupBox();
            this.tempOverviewLB = new System.Windows.Forms.Label();
            this.tempOverviewPB = new System.Windows.Forms.PictureBox();
            this.ammoniaOverviewPB = new System.Windows.Forms.PictureBox();
            this.nitriteOverviewPB = new System.Windows.Forms.PictureBox();
            this.nitrateOverviewPB = new System.Windows.Forms.PictureBox();
            this.pHOverviewPB = new System.Windows.Forms.PictureBox();
            this.nitrateOverviewLB = new System.Windows.Forms.Label();
            this.pHOverviewLB = new System.Windows.Forms.Label();
            this.nitriteOverviewLB = new System.Windows.Forms.Label();
            this.ammoniaOverviewLB = new System.Windows.Forms.Label();
            this.pondTP = new System.Windows.Forms.TabPage();
            this.feedingPondGB = new System.Windows.Forms.GroupBox();
            this.setTimesPondBT = new System.Windows.Forms.Button();
            this.donePondLB = new System.Windows.Forms.Label();
            this.feedNowPondBT = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.feedTimePondTrB = new System.Windows.Forms.TrackBar();
            this.correctionFactorPondLB = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.eveFeedDonePondChB = new System.Windows.Forms.CheckBox();
            this.mornFeedDonePondChB = new System.Windows.Forms.CheckBox();
            this.eveFeedHrPondTB = new System.Windows.Forms.TextBox();
            this.mornFeedHrPondTB = new System.Windows.Forms.TextBox();
            this.eveFeedMinPondTB = new System.Windows.Forms.TextBox();
            this.eveFeedPondLB = new System.Windows.Forms.Label();
            this.minutePondLB = new System.Windows.Forms.Label();
            this.hourPondLB = new System.Windows.Forms.Label();
            this.mornFeedMinPondTB = new System.Windows.Forms.TextBox();
            this.mornFeedPondLB = new System.Windows.Forms.Label();
            this.metadataPondGB = new System.Windows.Forms.GroupBox();
            this.growthPondChB = new System.Windows.Forms.CheckBox();
            this.breedingPondChB = new System.Windows.Forms.CheckBox();
            this.highSunPondChB = new System.Windows.Forms.CheckBox();
            this.plantedPondChB = new System.Windows.Forms.CheckBox();
            this.typePondLB = new System.Windows.Forms.Label();
            this.typePondCB = new System.Windows.Forms.ComboBox();
            this.techInfoPondGB = new System.Windows.Forms.GroupBox();
            this.fishSuitablePondPB = new System.Windows.Forms.PictureBox();
            this.recFoodPondPB = new System.Windows.Forms.PictureBox();
            this.linerSizePondPB = new System.Windows.Forms.PictureBox();
            this.uvRequiredPondPB = new System.Windows.Forms.PictureBox();
            this.turnoverRatePondPB = new System.Windows.Forms.PictureBox();
            this.volumeOverviewPondPB = new System.Windows.Forms.PictureBox();
            this.fishSuitablePondLB = new System.Windows.Forms.Label();
            this.recFoodPondLB = new System.Windows.Forms.Label();
            this.linerSizePondLB = new System.Windows.Forms.Label();
            this.uvRequiredPondLB = new System.Windows.Forms.Label();
            this.turnoverRatePondLB = new System.Windows.Forms.Label();
            this.volumeOverviewPondLB = new System.Windows.Forms.Label();
            this.dimensionsPondGB = new System.Windows.Forms.GroupBox();
            this.dimensionsPondBT = new System.Windows.Forms.Button();
            this.depthPondLB = new System.Windows.Forms.Label();
            this.lengthPondTB = new System.Windows.Forms.TextBox();
            this.widthPondLB = new System.Windows.Forms.Label();
            this.widthPondTB = new System.Windows.Forms.TextBox();
            this.lengthPondLB = new System.Windows.Forms.Label();
            this.depthPondTB = new System.Windows.Forms.TextBox();
            this.fishTP = new System.Windows.Forms.TabPage();
            this.addLivestockFishGB = new System.Windows.Forms.GroupBox();
            this.sizeFishTB = new System.Windows.Forms.TextBox();
            this.sizeFishLB = new System.Windows.Forms.Label();
            this.genderFishGB = new System.Windows.Forms.GroupBox();
            this.unknownFishRB = new System.Windows.Forms.RadioButton();
            this.femaleFishRB = new System.Windows.Forms.RadioButton();
            this.maleFishRB = new System.Windows.Forms.RadioButton();
            this.fishCB = new System.Windows.Forms.ComboBox();
            this.ageFishTB = new System.Windows.Forms.TextBox();
            this.commentsFishLB = new System.Windows.Forms.Label();
            this.addFishBT = new System.Windows.Forms.Button();
            this.commentsFishTB = new System.Windows.Forms.TextBox();
            this.removeFishBT = new System.Windows.Forms.Button();
            this.ageFishLB = new System.Windows.Forms.Label();
            this.speciesLB = new System.Windows.Forms.Label();
            this.tableFishDGV = new System.Windows.Forms.DataGridView();
            this.parametersTP = new System.Windows.Forms.TabPage();
            this.addRecordParametersGB = new System.Windows.Forms.GroupBox();
            this.csvWaterChemistryBT = new System.Windows.Forms.Button();
            this.temperatureParametersTB = new System.Windows.Forms.TextBox();
            this.temperatureParametersLB = new System.Windows.Forms.Label();
            this.ghParametersTB = new System.Windows.Forms.TextBox();
            this.removeRecordParametersBT = new System.Windows.Forms.Button();
            this.addRecordParametersBT = new System.Windows.Forms.Button();
            this.ghParametersLB = new System.Windows.Forms.Label();
            this.ammoniaParametersTB = new System.Windows.Forms.TextBox();
            this.khParametersLB = new System.Windows.Forms.Label();
            this.nitriteParametersTB = new System.Windows.Forms.TextBox();
            this.nitrateParametersLB = new System.Windows.Forms.Label();
            this.nitrateParametersTB = new System.Windows.Forms.TextBox();
            this.nitriteParametersLB = new System.Windows.Forms.Label();
            this.phParametersTB = new System.Windows.Forms.TextBox();
            this.ammoniaParametersLB = new System.Windows.Forms.Label();
            this.khParametersTB = new System.Windows.Forms.TextBox();
            this.phParametersLB = new System.Windows.Forms.Label();
            this.dateParametersDTP = new System.Windows.Forms.DateTimePicker();
            this.dateParametersLB = new System.Windows.Forms.Label();
            this.tableParametersDGV = new System.Windows.Forms.DataGridView();
            this.foodTP = new System.Windows.Forms.TabPage();
            this.addFoodGB = new System.Windows.Forms.GroupBox();
            this.sizeFoodGB = new System.Windows.Forms.GroupBox();
            this.pelletFoodRB = new System.Windows.Forms.RadioButton();
            this.flakeFoodRB = new System.Windows.Forms.RadioButton();
            this.sticksFoodRB = new System.Windows.Forms.RadioButton();
            this.nameFoodLB = new System.Windows.Forms.Label();
            this.removeFoodBT = new System.Windows.Forms.Button();
            this.addFoodBT = new System.Windows.Forms.Button();
            this.typeFoodGB = new System.Windows.Forms.GroupBox();
            this.wheatgermFoodRB = new System.Windows.Forms.RadioButton();
            this.stapleFoodRB = new System.Windows.Forms.RadioButton();
            this.proteinFoodRB = new System.Windows.Forms.RadioButton();
            this.commentsFoodTB = new System.Windows.Forms.TextBox();
            this.commentsFoodLB = new System.Windows.Forms.Label();
            this.nameFoodTB = new System.Windows.Forms.TextBox();
            this.tableFoodDGV = new System.Windows.Forms.DataGridView();
            this.powerTP = new System.Windows.Forms.TabPage();
            this.typeDeviceGB = new System.Windows.Forms.GroupBox();
            this.typeDeviceLB = new System.Windows.Forms.Label();
            this.typeDeviceCB = new System.Windows.Forms.ComboBox();
            this.nameDeviceLB = new System.Windows.Forms.Label();
            this.nameDeviceTB = new System.Windows.Forms.TextBox();
            this.loadDeviceBT = new System.Windows.Forms.Button();
            this.statusDeviceGB = new System.Windows.Forms.GroupBox();
            this.scheduledDeviceChB = new System.Windows.Forms.CheckBox();
            this.onDeviceRB = new System.Windows.Forms.RadioButton();
            this.offDeviceRB = new System.Windows.Forms.RadioButton();
            this.scheduleDeviceGB = new System.Windows.Forms.GroupBox();
            this.colon2LB = new System.Windows.Forms.Label();
            this.colonLB = new System.Windows.Forms.Label();
            this.finishDeviceLB = new System.Windows.Forms.Label();
            this.startDeviceLB = new System.Windows.Forms.Label();
            this.minDeviceLB = new System.Windows.Forms.Label();
            this.hourDeviceLB = new System.Windows.Forms.Label();
            this.finishMinDeviceTB = new System.Windows.Forms.TextBox();
            this.startMinDeviceTB = new System.Windows.Forms.TextBox();
            this.finishHrDeviceTB = new System.Windows.Forms.TextBox();
            this.startHrDeviceTB = new System.Windows.Forms.TextBox();
            this.removeDeviceBT = new System.Windows.Forms.Button();
            this.addDeviceBT = new System.Windows.Forms.Button();
            this.currentDevicesLB = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.navigationTC.SuspendLayout();
            this.overviewTP.SuspendLayout();
            this.maintenanceOverviewGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waterTestOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterCleanOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpCleanOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uvBulbOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterChangeOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockingOverviewPB)).BeginInit();
            this.controllerOverviewGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.devicesOverviewGB.SuspendLayout();
            this.parametersOverviewGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tempOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ammoniaOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nitriteOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nitrateOverviewPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pHOverviewPB)).BeginInit();
            this.pondTP.SuspendLayout();
            this.feedingPondGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.feedTimePondTrB)).BeginInit();
            this.metadataPondGB.SuspendLayout();
            this.techInfoPondGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fishSuitablePondPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.recFoodPondPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.linerSizePondPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uvRequiredPondPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turnoverRatePondPB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeOverviewPondPB)).BeginInit();
            this.dimensionsPondGB.SuspendLayout();
            this.fishTP.SuspendLayout();
            this.addLivestockFishGB.SuspendLayout();
            this.genderFishGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableFishDGV)).BeginInit();
            this.parametersTP.SuspendLayout();
            this.addRecordParametersGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableParametersDGV)).BeginInit();
            this.foodTP.SuspendLayout();
            this.addFoodGB.SuspendLayout();
            this.sizeFoodGB.SuspendLayout();
            this.typeFoodGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableFoodDGV)).BeginInit();
            this.powerTP.SuspendLayout();
            this.typeDeviceGB.SuspendLayout();
            this.statusDeviceGB.SuspendLayout();
            this.scheduleDeviceGB.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            this.SuspendLayout();
            // 
            // navigationTC
            // 
            this.navigationTC.Controls.Add(this.overviewTP);
            this.navigationTC.Controls.Add(this.pondTP);
            this.navigationTC.Controls.Add(this.fishTP);
            this.navigationTC.Controls.Add(this.parametersTP);
            this.navigationTC.Controls.Add(this.foodTP);
            this.navigationTC.Controls.Add(this.powerTP);
            this.navigationTC.Location = new System.Drawing.Point(3, 3);
            this.navigationTC.Name = "navigationTC";
            this.navigationTC.SelectedIndex = 0;
            this.navigationTC.Size = new System.Drawing.Size(682, 513);
            this.navigationTC.TabIndex = 1;
            this.navigationTC.SelectedIndexChanged += new System.EventHandler(this.SectionTab_SelectedIndexChanged);
            // 
            // overviewTP
            // 
            this.overviewTP.Controls.Add(this.maintenanceOverviewGB);
            this.overviewTP.Controls.Add(this.controllerOverviewGB);
            this.overviewTP.Controls.Add(this.devicesOverviewGB);
            this.overviewTP.Controls.Add(this.parametersOverviewGB);
            this.overviewTP.Location = new System.Drawing.Point(4, 22);
            this.overviewTP.Name = "overviewTP";
            this.overviewTP.Padding = new System.Windows.Forms.Padding(3);
            this.overviewTP.Size = new System.Drawing.Size(674, 487);
            this.overviewTP.TabIndex = 1;
            this.overviewTP.Text = "Overview";
            this.overviewTP.UseVisualStyleBackColor = true;
            // 
            // maintenanceOverviewGB
            // 
            this.maintenanceOverviewGB.Controls.Add(this.remindersOverviewLL);
            this.maintenanceOverviewGB.Controls.Add(this.waterTestOverviewPB);
            this.maintenanceOverviewGB.Controls.Add(this.filterCleanOverviewPB);
            this.maintenanceOverviewGB.Controls.Add(this.pumpCleanOverviewPB);
            this.maintenanceOverviewGB.Controls.Add(this.uvBulbOverviewPB);
            this.maintenanceOverviewGB.Controls.Add(this.waterChangeOverviewPB);
            this.maintenanceOverviewGB.Controls.Add(this.stockingOverviewPB);
            this.maintenanceOverviewGB.Controls.Add(this.waterTestOverviewLB);
            this.maintenanceOverviewGB.Controls.Add(this.filterCleanOverviewLB);
            this.maintenanceOverviewGB.Controls.Add(this.pumpCleanOverviewLB);
            this.maintenanceOverviewGB.Controls.Add(this.uvBulbOverviewLB);
            this.maintenanceOverviewGB.Controls.Add(this.waterChangeOverviewLB);
            this.maintenanceOverviewGB.Controls.Add(this.stockingOverviewLB);
            this.maintenanceOverviewGB.Location = new System.Drawing.Point(5, 224);
            this.maintenanceOverviewGB.Name = "maintenanceOverviewGB";
            this.maintenanceOverviewGB.Size = new System.Drawing.Size(660, 257);
            this.maintenanceOverviewGB.TabIndex = 6;
            this.maintenanceOverviewGB.TabStop = false;
            this.maintenanceOverviewGB.Text = "Summary";
            // 
            // remindersOverviewLL
            // 
            this.remindersOverviewLL.AutoSize = true;
            this.remindersOverviewLL.Location = new System.Drawing.Point(594, 16);
            this.remindersOverviewLL.Name = "remindersOverviewLL";
            this.remindersOverviewLL.Size = new System.Drawing.Size(57, 13);
            this.remindersOverviewLL.TabIndex = 4;
            this.remindersOverviewLL.TabStop = true;
            this.remindersOverviewLL.Text = "Reminders";
            this.remindersOverviewLL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.SetupLLB_LinkClicked);
            // 
            // waterTestOverviewPB
            // 
            this.waterTestOverviewPB.Image = global::PondManager.Properties.Resources.watertest;
            this.waterTestOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.waterTestOverviewPB.Location = new System.Drawing.Point(9, 211);
            this.waterTestOverviewPB.Name = "waterTestOverviewPB";
            this.waterTestOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.waterTestOverviewPB.TabIndex = 15;
            this.waterTestOverviewPB.TabStop = false;
            // 
            // filterCleanOverviewPB
            // 
            this.filterCleanOverviewPB.Image = global::PondManager.Properties.Resources.filter;
            this.filterCleanOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.filterCleanOverviewPB.Location = new System.Drawing.Point(9, 134);
            this.filterCleanOverviewPB.Name = "filterCleanOverviewPB";
            this.filterCleanOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.filterCleanOverviewPB.TabIndex = 14;
            this.filterCleanOverviewPB.TabStop = false;
            // 
            // pumpCleanOverviewPB
            // 
            this.pumpCleanOverviewPB.Image = global::PondManager.Properties.Resources.pump;
            this.pumpCleanOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pumpCleanOverviewPB.Location = new System.Drawing.Point(9, 96);
            this.pumpCleanOverviewPB.Name = "pumpCleanOverviewPB";
            this.pumpCleanOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.pumpCleanOverviewPB.TabIndex = 13;
            this.pumpCleanOverviewPB.TabStop = false;
            // 
            // uvBulbOverviewPB
            // 
            this.uvBulbOverviewPB.Image = global::PondManager.Properties.Resources.uv;
            this.uvBulbOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.uvBulbOverviewPB.Location = new System.Drawing.Point(9, 173);
            this.uvBulbOverviewPB.Name = "uvBulbOverviewPB";
            this.uvBulbOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.uvBulbOverviewPB.TabIndex = 12;
            this.uvBulbOverviewPB.TabStop = false;
            // 
            // waterChangeOverviewPB
            // 
            this.waterChangeOverviewPB.Image = global::PondManager.Properties.Resources.waterchange;
            this.waterChangeOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.waterChangeOverviewPB.Location = new System.Drawing.Point(9, 58);
            this.waterChangeOverviewPB.Name = "waterChangeOverviewPB";
            this.waterChangeOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.waterChangeOverviewPB.TabIndex = 11;
            this.waterChangeOverviewPB.TabStop = false;
            // 
            // stockingOverviewPB
            // 
            this.stockingOverviewPB.Image = global::PondManager.Properties.Resources.stocking;
            this.stockingOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.stockingOverviewPB.Location = new System.Drawing.Point(9, 20);
            this.stockingOverviewPB.Name = "stockingOverviewPB";
            this.stockingOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.stockingOverviewPB.TabIndex = 10;
            this.stockingOverviewPB.TabStop = false;
            // 
            // waterTestOverviewLB
            // 
            this.waterTestOverviewLB.AutoSize = true;
            this.waterTestOverviewLB.Location = new System.Drawing.Point(47, 221);
            this.waterTestOverviewLB.Name = "waterTestOverviewLB";
            this.waterTestOverviewLB.Size = new System.Drawing.Size(309, 13);
            this.waterTestOverviewLB.TabIndex = 5;
            this.waterTestOverviewLB.Text = "your next scheduled water test is in xx days xx weeks, on dd-mm";
            // 
            // filterCleanOverviewLB
            // 
            this.filterCleanOverviewLB.AutoSize = true;
            this.filterCleanOverviewLB.Location = new System.Drawing.Point(47, 144);
            this.filterCleanOverviewLB.Name = "filterCleanOverviewLB";
            this.filterCleanOverviewLB.Size = new System.Drawing.Size(244, 13);
            this.filterCleanOverviewLB.TabIndex = 4;
            this.filterCleanOverviewLB.Text = "filter clean required in xx days xx weeks, on dd-mm";
            // 
            // pumpCleanOverviewLB
            // 
            this.pumpCleanOverviewLB.AutoSize = true;
            this.pumpCleanOverviewLB.Location = new System.Drawing.Point(47, 105);
            this.pumpCleanOverviewLB.Name = "pumpCleanOverviewLB";
            this.pumpCleanOverviewLB.Size = new System.Drawing.Size(251, 13);
            this.pumpCleanOverviewLB.TabIndex = 3;
            this.pumpCleanOverviewLB.Text = "pump clean required in xx days xx weeks, on dd-mm";
            // 
            // uvBulbOverviewLB
            // 
            this.uvBulbOverviewLB.AutoSize = true;
            this.uvBulbOverviewLB.Location = new System.Drawing.Point(47, 183);
            this.uvBulbOverviewLB.Name = "uvBulbOverviewLB";
            this.uvBulbOverviewLB.Size = new System.Drawing.Size(333, 13);
            this.uvBulbOverviewLB.TabIndex = 2;
            this.uvBulbOverviewLB.Text = "uv clarifier bulb is due for replacement in xx days xx weeks, on dd-mm";
            // 
            // waterChangeOverviewLB
            // 
            this.waterChangeOverviewLB.AutoSize = true;
            this.waterChangeOverviewLB.Location = new System.Drawing.Point(47, 68);
            this.waterChangeOverviewLB.Name = "waterChangeOverviewLB";
            this.waterChangeOverviewLB.Size = new System.Drawing.Size(245, 13);
            this.waterChangeOverviewLB.TabIndex = 1;
            this.waterChangeOverviewLB.Text = "the next water change is due in xx days, on dd-mm";
            // 
            // stockingOverviewLB
            // 
            this.stockingOverviewLB.AutoSize = true;
            this.stockingOverviewLB.Location = new System.Drawing.Point(47, 29);
            this.stockingOverviewLB.Name = "stockingOverviewLB";
            this.stockingOverviewLB.Size = new System.Drawing.Size(179, 13);
            this.stockingOverviewLB.TabIndex = 0;
            this.stockingOverviewLB.Text = "this pond is currently at xx% stocking";
            // 
            // controllerOverviewGB
            // 
            this.controllerOverviewGB.Controls.Add(this.pictureBox1);
            this.controllerOverviewGB.Controls.Add(this.pictureBox2);
            this.controllerOverviewGB.Controls.Add(this.timeOverviewLB);
            this.controllerOverviewGB.Controls.Add(this.connStatusOverviewLB);
            this.controllerOverviewGB.Controls.Add(this.portOverviewLB);
            this.controllerOverviewGB.Controls.Add(this.ipOverviewLB);
            this.controllerOverviewGB.Controls.Add(this.portColonLB);
            this.controllerOverviewGB.Controls.Add(this.portOverviewTB);
            this.controllerOverviewGB.Controls.Add(this.ipOverviewTB);
            this.controllerOverviewGB.Controls.Add(this.disconnectOverviewBT);
            this.controllerOverviewGB.Controls.Add(this.connectOverviewBT);
            this.controllerOverviewGB.Location = new System.Drawing.Point(466, 4);
            this.controllerOverviewGB.Name = "controllerOverviewGB";
            this.controllerOverviewGB.Size = new System.Drawing.Size(200, 214);
            this.controllerOverviewGB.TabIndex = 5;
            this.controllerOverviewGB.TabStop = false;
            this.controllerOverviewGB.Text = "Controller Status";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::PondManager.Properties.Resources.wifi;
            this.pictureBox1.Location = new System.Drawing.Point(11, 120);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::PondManager.Properties.Resources.clock;
            this.pictureBox2.Location = new System.Drawing.Point(11, 83);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // timeOverviewLB
            // 
            this.timeOverviewLB.AutoSize = true;
            this.timeOverviewLB.Location = new System.Drawing.Point(49, 94);
            this.timeOverviewLB.Name = "timeOverviewLB";
            this.timeOverviewLB.Size = new System.Drawing.Size(121, 13);
            this.timeOverviewLB.TabIndex = 8;
            this.timeOverviewLB.Text = "Local Time: Unavailable";
            // 
            // connStatusOverviewLB
            // 
            this.connStatusOverviewLB.AutoSize = true;
            this.connStatusOverviewLB.Location = new System.Drawing.Point(48, 129);
            this.connStatusOverviewLB.Name = "connStatusOverviewLB";
            this.connStatusOverviewLB.Size = new System.Drawing.Size(146, 13);
            this.connStatusOverviewLB.TabIndex = 7;
            this.connStatusOverviewLB.Text = "Current Status: Disconnected";
            // 
            // portOverviewLB
            // 
            this.portOverviewLB.AutoSize = true;
            this.portOverviewLB.Location = new System.Drawing.Point(134, 25);
            this.portOverviewLB.Name = "portOverviewLB";
            this.portOverviewLB.Size = new System.Drawing.Size(26, 13);
            this.portOverviewLB.TabIndex = 6;
            this.portOverviewLB.Text = "Port";
            // 
            // ipOverviewLB
            // 
            this.ipOverviewLB.AutoSize = true;
            this.ipOverviewLB.Location = new System.Drawing.Point(8, 25);
            this.ipOverviewLB.Name = "ipOverviewLB";
            this.ipOverviewLB.Size = new System.Drawing.Size(58, 13);
            this.ipOverviewLB.TabIndex = 5;
            this.ipOverviewLB.Text = "IP Address";
            // 
            // portColonLB
            // 
            this.portColonLB.AutoSize = true;
            this.portColonLB.Location = new System.Drawing.Point(118, 47);
            this.portColonLB.Name = "portColonLB";
            this.portColonLB.Size = new System.Drawing.Size(10, 13);
            this.portColonLB.TabIndex = 4;
            this.portColonLB.Text = ":";
            // 
            // portOverviewTB
            // 
            this.portOverviewTB.Location = new System.Drawing.Point(135, 44);
            this.portOverviewTB.Name = "portOverviewTB";
            this.portOverviewTB.Size = new System.Drawing.Size(49, 20);
            this.portOverviewTB.TabIndex = 1;
            // 
            // ipOverviewTB
            // 
            this.ipOverviewTB.Location = new System.Drawing.Point(11, 44);
            this.ipOverviewTB.Name = "ipOverviewTB";
            this.ipOverviewTB.Size = new System.Drawing.Size(100, 20);
            this.ipOverviewTB.TabIndex = 0;
            // 
            // disconnectOverviewBT
            // 
            this.disconnectOverviewBT.Enabled = false;
            this.disconnectOverviewBT.Location = new System.Drawing.Point(109, 180);
            this.disconnectOverviewBT.Name = "disconnectOverviewBT";
            this.disconnectOverviewBT.Size = new System.Drawing.Size(75, 23);
            this.disconnectOverviewBT.TabIndex = 3;
            this.disconnectOverviewBT.Text = "Disconnect";
            this.disconnectOverviewBT.UseVisualStyleBackColor = true;
            this.disconnectOverviewBT.Click += new System.EventHandler(this.DisconnectBT_Click);
            // 
            // connectOverviewBT
            // 
            this.connectOverviewBT.Location = new System.Drawing.Point(16, 180);
            this.connectOverviewBT.Name = "connectOverviewBT";
            this.connectOverviewBT.Size = new System.Drawing.Size(75, 23);
            this.connectOverviewBT.TabIndex = 2;
            this.connectOverviewBT.Text = "Connect";
            this.connectOverviewBT.UseVisualStyleBackColor = true;
            this.connectOverviewBT.Click += new System.EventHandler(this.ConnectBT_Click);
            // 
            // devicesOverviewGB
            // 
            this.devicesOverviewGB.Controls.Add(this.device5OverviewLB);
            this.devicesOverviewGB.Controls.Add(this.device3OverviewLB);
            this.devicesOverviewGB.Controls.Add(this.device1OverviewLB);
            this.devicesOverviewGB.Controls.Add(this.device2OverviewLB);
            this.devicesOverviewGB.Controls.Add(this.device4OverviewLB);
            this.devicesOverviewGB.Location = new System.Drawing.Point(211, 4);
            this.devicesOverviewGB.Name = "devicesOverviewGB";
            this.devicesOverviewGB.Size = new System.Drawing.Size(249, 214);
            this.devicesOverviewGB.TabIndex = 4;
            this.devicesOverviewGB.TabStop = false;
            this.devicesOverviewGB.Text = "Running Devices";
            // 
            // device5OverviewLB
            // 
            this.device5OverviewLB.AutoSize = true;
            this.device5OverviewLB.Location = new System.Drawing.Point(50, 176);
            this.device5OverviewLB.Name = "device5OverviewLB";
            this.device5OverviewLB.Size = new System.Drawing.Size(50, 13);
            this.device5OverviewLB.TabIndex = 4;
            this.device5OverviewLB.Text = "Device 5";
            // 
            // device3OverviewLB
            // 
            this.device3OverviewLB.AutoSize = true;
            this.device3OverviewLB.Location = new System.Drawing.Point(50, 106);
            this.device3OverviewLB.Name = "device3OverviewLB";
            this.device3OverviewLB.Size = new System.Drawing.Size(50, 13);
            this.device3OverviewLB.TabIndex = 3;
            this.device3OverviewLB.Text = "Device 3";
            // 
            // device1OverviewLB
            // 
            this.device1OverviewLB.AutoSize = true;
            this.device1OverviewLB.Location = new System.Drawing.Point(50, 34);
            this.device1OverviewLB.Name = "device1OverviewLB";
            this.device1OverviewLB.Size = new System.Drawing.Size(50, 13);
            this.device1OverviewLB.TabIndex = 2;
            this.device1OverviewLB.Text = "Device 1";
            // 
            // device2OverviewLB
            // 
            this.device2OverviewLB.AutoSize = true;
            this.device2OverviewLB.Location = new System.Drawing.Point(50, 70);
            this.device2OverviewLB.Name = "device2OverviewLB";
            this.device2OverviewLB.Size = new System.Drawing.Size(50, 13);
            this.device2OverviewLB.TabIndex = 1;
            this.device2OverviewLB.Text = "Device 2";
            // 
            // device4OverviewLB
            // 
            this.device4OverviewLB.AutoSize = true;
            this.device4OverviewLB.Location = new System.Drawing.Point(50, 141);
            this.device4OverviewLB.Name = "device4OverviewLB";
            this.device4OverviewLB.Size = new System.Drawing.Size(50, 13);
            this.device4OverviewLB.TabIndex = 0;
            this.device4OverviewLB.Text = "Device 4";
            // 
            // parametersOverviewGB
            // 
            this.parametersOverviewGB.Controls.Add(this.tempOverviewLB);
            this.parametersOverviewGB.Controls.Add(this.tempOverviewPB);
            this.parametersOverviewGB.Controls.Add(this.ammoniaOverviewPB);
            this.parametersOverviewGB.Controls.Add(this.nitriteOverviewPB);
            this.parametersOverviewGB.Controls.Add(this.nitrateOverviewPB);
            this.parametersOverviewGB.Controls.Add(this.pHOverviewPB);
            this.parametersOverviewGB.Controls.Add(this.nitrateOverviewLB);
            this.parametersOverviewGB.Controls.Add(this.pHOverviewLB);
            this.parametersOverviewGB.Controls.Add(this.nitriteOverviewLB);
            this.parametersOverviewGB.Controls.Add(this.ammoniaOverviewLB);
            this.parametersOverviewGB.Location = new System.Drawing.Point(5, 4);
            this.parametersOverviewGB.Name = "parametersOverviewGB";
            this.parametersOverviewGB.Size = new System.Drawing.Size(200, 214);
            this.parametersOverviewGB.TabIndex = 0;
            this.parametersOverviewGB.TabStop = false;
            this.parametersOverviewGB.Text = "Current Parameters";
            // 
            // tempOverviewLB
            // 
            this.tempOverviewLB.AutoSize = true;
            this.tempOverviewLB.Location = new System.Drawing.Point(47, 30);
            this.tempOverviewLB.Name = "tempOverviewLB";
            this.tempOverviewLB.Size = new System.Drawing.Size(67, 13);
            this.tempOverviewLB.TabIndex = 9;
            this.tempOverviewLB.Text = "Temperature";
            // 
            // tempOverviewPB
            // 
            this.tempOverviewPB.Image = global::PondManager.Properties.Resources.temp;
            this.tempOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.tempOverviewPB.Location = new System.Drawing.Point(9, 19);
            this.tempOverviewPB.Name = "tempOverviewPB";
            this.tempOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.tempOverviewPB.TabIndex = 8;
            this.tempOverviewPB.TabStop = false;
            // 
            // ammoniaOverviewPB
            // 
            this.ammoniaOverviewPB.Image = global::PondManager.Properties.Resources.ammonia;
            this.ammoniaOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.ammoniaOverviewPB.Location = new System.Drawing.Point(9, 95);
            this.ammoniaOverviewPB.Name = "ammoniaOverviewPB";
            this.ammoniaOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.ammoniaOverviewPB.TabIndex = 7;
            this.ammoniaOverviewPB.TabStop = false;
            // 
            // nitriteOverviewPB
            // 
            this.nitriteOverviewPB.Image = global::PondManager.Properties.Resources.nitrite;
            this.nitriteOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.nitriteOverviewPB.Location = new System.Drawing.Point(9, 133);
            this.nitriteOverviewPB.Name = "nitriteOverviewPB";
            this.nitriteOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.nitriteOverviewPB.TabIndex = 6;
            this.nitriteOverviewPB.TabStop = false;
            // 
            // nitrateOverviewPB
            // 
            this.nitrateOverviewPB.Image = global::PondManager.Properties.Resources.nitrate;
            this.nitrateOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.nitrateOverviewPB.Location = new System.Drawing.Point(9, 171);
            this.nitrateOverviewPB.Name = "nitrateOverviewPB";
            this.nitrateOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.nitrateOverviewPB.TabIndex = 5;
            this.nitrateOverviewPB.TabStop = false;
            // 
            // pHOverviewPB
            // 
            this.pHOverviewPB.Image = global::PondManager.Properties.Resources.ph;
            this.pHOverviewPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pHOverviewPB.Location = new System.Drawing.Point(9, 57);
            this.pHOverviewPB.Name = "pHOverviewPB";
            this.pHOverviewPB.Size = new System.Drawing.Size(32, 32);
            this.pHOverviewPB.TabIndex = 4;
            this.pHOverviewPB.TabStop = false;
            // 
            // nitrateOverviewLB
            // 
            this.nitrateOverviewLB.AutoSize = true;
            this.nitrateOverviewLB.Location = new System.Drawing.Point(47, 179);
            this.nitrateOverviewLB.Name = "nitrateOverviewLB";
            this.nitrateOverviewLB.Size = new System.Drawing.Size(38, 13);
            this.nitrateOverviewLB.TabIndex = 3;
            this.nitrateOverviewLB.Text = "Nitrate";
            // 
            // pHOverviewLB
            // 
            this.pHOverviewLB.AutoSize = true;
            this.pHOverviewLB.Location = new System.Drawing.Point(47, 66);
            this.pHOverviewLB.Name = "pHOverviewLB";
            this.pHOverviewLB.Size = new System.Drawing.Size(21, 13);
            this.pHOverviewLB.TabIndex = 2;
            this.pHOverviewLB.Text = "pH";
            // 
            // nitriteOverviewLB
            // 
            this.nitriteOverviewLB.AutoSize = true;
            this.nitriteOverviewLB.Location = new System.Drawing.Point(47, 143);
            this.nitriteOverviewLB.Name = "nitriteOverviewLB";
            this.nitriteOverviewLB.Size = new System.Drawing.Size(34, 13);
            this.nitriteOverviewLB.TabIndex = 1;
            this.nitriteOverviewLB.Text = "Nitrite";
            // 
            // ammoniaOverviewLB
            // 
            this.ammoniaOverviewLB.AutoSize = true;
            this.ammoniaOverviewLB.Location = new System.Drawing.Point(47, 105);
            this.ammoniaOverviewLB.Name = "ammoniaOverviewLB";
            this.ammoniaOverviewLB.Size = new System.Drawing.Size(50, 13);
            this.ammoniaOverviewLB.TabIndex = 0;
            this.ammoniaOverviewLB.Text = "Ammonia";
            // 
            // pondTP
            // 
            this.pondTP.Controls.Add(this.feedingPondGB);
            this.pondTP.Controls.Add(this.metadataPondGB);
            this.pondTP.Controls.Add(this.techInfoPondGB);
            this.pondTP.Controls.Add(this.dimensionsPondGB);
            this.pondTP.Location = new System.Drawing.Point(4, 22);
            this.pondTP.Name = "pondTP";
            this.pondTP.Size = new System.Drawing.Size(674, 487);
            this.pondTP.TabIndex = 5;
            this.pondTP.Text = "Pond";
            this.pondTP.UseVisualStyleBackColor = true;
            // 
            // feedingPondGB
            // 
            this.feedingPondGB.Controls.Add(this.setTimesPondBT);
            this.feedingPondGB.Controls.Add(this.donePondLB);
            this.feedingPondGB.Controls.Add(this.feedNowPondBT);
            this.feedingPondGB.Controls.Add(this.label1);
            this.feedingPondGB.Controls.Add(this.feedTimePondTrB);
            this.feedingPondGB.Controls.Add(this.correctionFactorPondLB);
            this.feedingPondGB.Controls.Add(this.label13);
            this.feedingPondGB.Controls.Add(this.label12);
            this.feedingPondGB.Controls.Add(this.eveFeedDonePondChB);
            this.feedingPondGB.Controls.Add(this.mornFeedDonePondChB);
            this.feedingPondGB.Controls.Add(this.eveFeedHrPondTB);
            this.feedingPondGB.Controls.Add(this.mornFeedHrPondTB);
            this.feedingPondGB.Controls.Add(this.eveFeedMinPondTB);
            this.feedingPondGB.Controls.Add(this.eveFeedPondLB);
            this.feedingPondGB.Controls.Add(this.minutePondLB);
            this.feedingPondGB.Controls.Add(this.hourPondLB);
            this.feedingPondGB.Controls.Add(this.mornFeedMinPondTB);
            this.feedingPondGB.Controls.Add(this.mornFeedPondLB);
            this.feedingPondGB.Location = new System.Drawing.Point(380, 4);
            this.feedingPondGB.Name = "feedingPondGB";
            this.feedingPondGB.Size = new System.Drawing.Size(289, 220);
            this.feedingPondGB.TabIndex = 36;
            this.feedingPondGB.TabStop = false;
            this.feedingPondGB.Text = "Feeding";
            // 
            // setTimesPondBT
            // 
            this.setTimesPondBT.Location = new System.Drawing.Point(63, 177);
            this.setTimesPondBT.Name = "setTimesPondBT";
            this.setTimesPondBT.Size = new System.Drawing.Size(75, 23);
            this.setTimesPondBT.TabIndex = 14;
            this.setTimesPondBT.Text = "Set Times";
            this.setTimesPondBT.UseVisualStyleBackColor = true;
            this.setTimesPondBT.Click += new System.EventHandler(this.setTimesPondBT_Click);
            // 
            // donePondLB
            // 
            this.donePondLB.AutoSize = true;
            this.donePondLB.Location = new System.Drawing.Point(228, 28);
            this.donePondLB.Name = "donePondLB";
            this.donePondLB.Size = new System.Drawing.Size(33, 13);
            this.donePondLB.TabIndex = 53;
            this.donePondLB.Text = "Done";
            // 
            // feedNowPondBT
            // 
            this.feedNowPondBT.Location = new System.Drawing.Point(153, 177);
            this.feedNowPondBT.Name = "feedNowPondBT";
            this.feedNowPondBT.Size = new System.Drawing.Size(75, 23);
            this.feedNowPondBT.TabIndex = 15;
            this.feedNowPondBT.Text = "Feed Now";
            this.feedNowPondBT.UseVisualStyleBackColor = true;
            this.feedNowPondBT.Click += new System.EventHandler(this.feedNowPondBT_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 159);
            this.label1.MaximumSize = new System.Drawing.Size(280, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 51;
            // 
            // feedTimePondTrB
            // 
            this.feedTimePondTrB.BackColor = System.Drawing.SystemColors.Window;
            this.feedTimePondTrB.LargeChange = 10;
            this.feedTimePondTrB.Location = new System.Drawing.Point(116, 132);
            this.feedTimePondTrB.Maximum = 3000;
            this.feedTimePondTrB.Minimum = 1000;
            this.feedTimePondTrB.Name = "feedTimePondTrB";
            this.feedTimePondTrB.Size = new System.Drawing.Size(136, 45);
            this.feedTimePondTrB.SmallChange = 5;
            this.feedTimePondTrB.TabIndex = 13;
            this.feedTimePondTrB.TickStyle = System.Windows.Forms.TickStyle.None;
            this.feedTimePondTrB.Value = 1000;
            this.feedTimePondTrB.ValueChanged += new System.EventHandler(this.FeedAmountSR_ValueChanged);
            // 
            // correctionFactorPondLB
            // 
            this.correctionFactorPondLB.AutoSize = true;
            this.correctionFactorPondLB.Location = new System.Drawing.Point(10, 134);
            this.correctionFactorPondLB.Name = "correctionFactorPondLB";
            this.correctionFactorPondLB.Size = new System.Drawing.Size(103, 13);
            this.correctionFactorPondLB.TabIndex = 50;
            this.correctionFactorPondLB.Text = "Feed Duration (1.0s)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(159, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(10, 13);
            this.label13.TabIndex = 48;
            this.label13.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(159, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = ":";
            // 
            // eveFeedDonePondChB
            // 
            this.eveFeedDonePondChB.AutoSize = true;
            this.eveFeedDonePondChB.Location = new System.Drawing.Point(237, 94);
            this.eveFeedDonePondChB.Name = "eveFeedDonePondChB";
            this.eveFeedDonePondChB.Size = new System.Drawing.Size(15, 14);
            this.eveFeedDonePondChB.TabIndex = 46;
            this.eveFeedDonePondChB.UseVisualStyleBackColor = true;
            this.eveFeedDonePondChB.CheckedChanged += new System.EventHandler(this.eveFeedDonePondChB_CheckedChanged);
            // 
            // mornFeedDonePondChB
            // 
            this.mornFeedDonePondChB.AutoSize = true;
            this.mornFeedDonePondChB.Location = new System.Drawing.Point(237, 51);
            this.mornFeedDonePondChB.Name = "mornFeedDonePondChB";
            this.mornFeedDonePondChB.Size = new System.Drawing.Size(15, 14);
            this.mornFeedDonePondChB.TabIndex = 45;
            this.mornFeedDonePondChB.UseVisualStyleBackColor = true;
            this.mornFeedDonePondChB.CheckedChanged += new System.EventHandler(this.mornFeedDonePondChB_CheckedChanged);
            // 
            // eveFeedHrPondTB
            // 
            this.eveFeedHrPondTB.Location = new System.Drawing.Point(116, 90);
            this.eveFeedHrPondTB.Name = "eveFeedHrPondTB";
            this.eveFeedHrPondTB.Size = new System.Drawing.Size(41, 20);
            this.eveFeedHrPondTB.TabIndex = 11;
            this.eveFeedHrPondTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FeedTimeTB_KeyDown);
            // 
            // mornFeedHrPondTB
            // 
            this.mornFeedHrPondTB.Location = new System.Drawing.Point(116, 47);
            this.mornFeedHrPondTB.Name = "mornFeedHrPondTB";
            this.mornFeedHrPondTB.Size = new System.Drawing.Size(41, 20);
            this.mornFeedHrPondTB.TabIndex = 9;
            this.mornFeedHrPondTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FeedTimeTB_KeyDown);
            // 
            // eveFeedMinPondTB
            // 
            this.eveFeedMinPondTB.Location = new System.Drawing.Point(171, 90);
            this.eveFeedMinPondTB.Name = "eveFeedMinPondTB";
            this.eveFeedMinPondTB.Size = new System.Drawing.Size(41, 20);
            this.eveFeedMinPondTB.TabIndex = 12;
            this.eveFeedMinPondTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FeedTimeTB_KeyDown);
            // 
            // eveFeedPondLB
            // 
            this.eveFeedPondLB.AutoSize = true;
            this.eveFeedPondLB.Location = new System.Drawing.Point(34, 94);
            this.eveFeedPondLB.Name = "eveFeedPondLB";
            this.eveFeedPondLB.Size = new System.Drawing.Size(73, 13);
            this.eveFeedPondLB.TabIndex = 40;
            this.eveFeedPondLB.Text = "Evening Feed";
            // 
            // minutePondLB
            // 
            this.minutePondLB.AutoSize = true;
            this.minutePondLB.Location = new System.Drawing.Point(169, 28);
            this.minutePondLB.Name = "minutePondLB";
            this.minutePondLB.Size = new System.Drawing.Size(39, 13);
            this.minutePondLB.TabIndex = 39;
            this.minutePondLB.Text = "Minute";
            // 
            // hourPondLB
            // 
            this.hourPondLB.AutoSize = true;
            this.hourPondLB.Location = new System.Drawing.Point(113, 28);
            this.hourPondLB.Name = "hourPondLB";
            this.hourPondLB.Size = new System.Drawing.Size(30, 13);
            this.hourPondLB.TabIndex = 38;
            this.hourPondLB.Text = "Hour";
            // 
            // mornFeedMinPondTB
            // 
            this.mornFeedMinPondTB.Location = new System.Drawing.Point(169, 47);
            this.mornFeedMinPondTB.Name = "mornFeedMinPondTB";
            this.mornFeedMinPondTB.Size = new System.Drawing.Size(41, 20);
            this.mornFeedMinPondTB.TabIndex = 10;
            this.mornFeedMinPondTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FeedTimeTB_KeyDown);
            // 
            // mornFeedPondLB
            // 
            this.mornFeedPondLB.AutoSize = true;
            this.mornFeedPondLB.Location = new System.Drawing.Point(35, 49);
            this.mornFeedPondLB.Name = "mornFeedPondLB";
            this.mornFeedPondLB.Size = new System.Drawing.Size(72, 13);
            this.mornFeedPondLB.TabIndex = 30;
            this.mornFeedPondLB.Text = "Morning Feed";
            // 
            // metadataPondGB
            // 
            this.metadataPondGB.Controls.Add(this.growthPondChB);
            this.metadataPondGB.Controls.Add(this.breedingPondChB);
            this.metadataPondGB.Controls.Add(this.highSunPondChB);
            this.metadataPondGB.Controls.Add(this.plantedPondChB);
            this.metadataPondGB.Controls.Add(this.typePondLB);
            this.metadataPondGB.Controls.Add(this.typePondCB);
            this.metadataPondGB.Location = new System.Drawing.Point(169, 4);
            this.metadataPondGB.Name = "metadataPondGB";
            this.metadataPondGB.Size = new System.Drawing.Size(205, 220);
            this.metadataPondGB.TabIndex = 32;
            this.metadataPondGB.TabStop = false;
            this.metadataPondGB.Text = "Information";
            // 
            // growthPondChB
            // 
            this.growthPondChB.AutoSize = true;
            this.growthPondChB.Location = new System.Drawing.Point(25, 181);
            this.growthPondChB.Name = "growthPondChB";
            this.growthPondChB.Size = new System.Drawing.Size(83, 17);
            this.growthPondChB.TabIndex = 8;
            this.growthPondChB.Text = "High growth";
            this.growthPondChB.UseVisualStyleBackColor = true;
            this.growthPondChB.CheckedChanged += new System.EventHandler(this.TypePondCB_SelectionChangeCommitted);
            // 
            // breedingPondChB
            // 
            this.breedingPondChB.AutoSize = true;
            this.breedingPondChB.Location = new System.Drawing.Point(25, 150);
            this.breedingPondChB.Name = "breedingPondChB";
            this.breedingPondChB.Size = new System.Drawing.Size(68, 17);
            this.breedingPondChB.TabIndex = 7;
            this.breedingPondChB.Text = "Breeding";
            this.breedingPondChB.UseVisualStyleBackColor = true;
            this.breedingPondChB.CheckedChanged += new System.EventHandler(this.TypePondCB_SelectionChangeCommitted);
            // 
            // highSunPondChB
            // 
            this.highSunPondChB.AutoSize = true;
            this.highSunPondChB.Location = new System.Drawing.Point(25, 118);
            this.highSunPondChB.Name = "highSunPondChB";
            this.highSunPondChB.Size = new System.Drawing.Size(114, 17);
            this.highSunPondChB.TabIndex = 6;
            this.highSunPondChB.Text = "High sun exposure";
            this.highSunPondChB.UseVisualStyleBackColor = true;
            this.highSunPondChB.CheckedChanged += new System.EventHandler(this.TypePondCB_SelectionChangeCommitted);
            // 
            // plantedPondChB
            // 
            this.plantedPondChB.AutoSize = true;
            this.plantedPondChB.Location = new System.Drawing.Point(25, 85);
            this.plantedPondChB.Name = "plantedPondChB";
            this.plantedPondChB.Size = new System.Drawing.Size(62, 17);
            this.plantedPondChB.TabIndex = 5;
            this.plantedPondChB.Text = "Planted";
            this.plantedPondChB.UseVisualStyleBackColor = true;
            this.plantedPondChB.CheckedChanged += new System.EventHandler(this.TypePondCB_SelectionChangeCommitted);
            // 
            // typePondLB
            // 
            this.typePondLB.AutoSize = true;
            this.typePondLB.Location = new System.Drawing.Point(22, 27);
            this.typePondLB.Name = "typePondLB";
            this.typePondLB.Size = new System.Drawing.Size(31, 13);
            this.typePondLB.TabIndex = 30;
            this.typePondLB.Text = "Type";
            // 
            // typePondCB
            // 
            this.typePondCB.FormattingEnabled = true;
            this.typePondCB.Location = new System.Drawing.Point(25, 46);
            this.typePondCB.Name = "typePondCB";
            this.typePondCB.Size = new System.Drawing.Size(152, 21);
            this.typePondCB.TabIndex = 4;
            this.typePondCB.SelectedValueChanged += new System.EventHandler(this.TypePondCB_SelectionChangeCommitted);
            // 
            // techInfoPondGB
            // 
            this.techInfoPondGB.Controls.Add(this.fishSuitablePondPB);
            this.techInfoPondGB.Controls.Add(this.recFoodPondPB);
            this.techInfoPondGB.Controls.Add(this.linerSizePondPB);
            this.techInfoPondGB.Controls.Add(this.uvRequiredPondPB);
            this.techInfoPondGB.Controls.Add(this.turnoverRatePondPB);
            this.techInfoPondGB.Controls.Add(this.volumeOverviewPondPB);
            this.techInfoPondGB.Controls.Add(this.fishSuitablePondLB);
            this.techInfoPondGB.Controls.Add(this.recFoodPondLB);
            this.techInfoPondGB.Controls.Add(this.linerSizePondLB);
            this.techInfoPondGB.Controls.Add(this.uvRequiredPondLB);
            this.techInfoPondGB.Controls.Add(this.turnoverRatePondLB);
            this.techInfoPondGB.Controls.Add(this.volumeOverviewPondLB);
            this.techInfoPondGB.Location = new System.Drawing.Point(5, 230);
            this.techInfoPondGB.Name = "techInfoPondGB";
            this.techInfoPondGB.Size = new System.Drawing.Size(664, 251);
            this.techInfoPondGB.TabIndex = 31;
            this.techInfoPondGB.TabStop = false;
            this.techInfoPondGB.Text = "Technical Information";
            // 
            // fishSuitablePondPB
            // 
            this.fishSuitablePondPB.Image = global::PondManager.Properties.Resources.stocking;
            this.fishSuitablePondPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.fishSuitablePondPB.Location = new System.Drawing.Point(9, 210);
            this.fishSuitablePondPB.Name = "fishSuitablePondPB";
            this.fishSuitablePondPB.Size = new System.Drawing.Size(32, 32);
            this.fishSuitablePondPB.TabIndex = 21;
            this.fishSuitablePondPB.TabStop = false;
            // 
            // recFoodPondPB
            // 
            this.recFoodPondPB.Image = global::PondManager.Properties.Resources.food;
            this.recFoodPondPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.recFoodPondPB.Location = new System.Drawing.Point(9, 172);
            this.recFoodPondPB.Name = "recFoodPondPB";
            this.recFoodPondPB.Size = new System.Drawing.Size(32, 32);
            this.recFoodPondPB.TabIndex = 20;
            this.recFoodPondPB.TabStop = false;
            // 
            // linerSizePondPB
            // 
            this.linerSizePondPB.Image = global::PondManager.Properties.Resources.liner;
            this.linerSizePondPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.linerSizePondPB.Location = new System.Drawing.Point(9, 132);
            this.linerSizePondPB.Name = "linerSizePondPB";
            this.linerSizePondPB.Size = new System.Drawing.Size(32, 32);
            this.linerSizePondPB.TabIndex = 19;
            this.linerSizePondPB.TabStop = false;
            // 
            // uvRequiredPondPB
            // 
            this.uvRequiredPondPB.Image = global::PondManager.Properties.Resources.uv;
            this.uvRequiredPondPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.uvRequiredPondPB.Location = new System.Drawing.Point(9, 95);
            this.uvRequiredPondPB.Name = "uvRequiredPondPB";
            this.uvRequiredPondPB.Size = new System.Drawing.Size(32, 32);
            this.uvRequiredPondPB.TabIndex = 18;
            this.uvRequiredPondPB.TabStop = false;
            // 
            // turnoverRatePondPB
            // 
            this.turnoverRatePondPB.Image = global::PondManager.Properties.Resources.waterchange;
            this.turnoverRatePondPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.turnoverRatePondPB.Location = new System.Drawing.Point(9, 57);
            this.turnoverRatePondPB.Name = "turnoverRatePondPB";
            this.turnoverRatePondPB.Size = new System.Drawing.Size(32, 32);
            this.turnoverRatePondPB.TabIndex = 17;
            this.turnoverRatePondPB.TabStop = false;
            // 
            // volumeOverviewPondPB
            // 
            this.volumeOverviewPondPB.Image = global::PondManager.Properties.Resources.volume;
            this.volumeOverviewPondPB.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.volumeOverviewPondPB.Location = new System.Drawing.Point(9, 19);
            this.volumeOverviewPondPB.Name = "volumeOverviewPondPB";
            this.volumeOverviewPondPB.Size = new System.Drawing.Size(32, 32);
            this.volumeOverviewPondPB.TabIndex = 16;
            this.volumeOverviewPondPB.TabStop = false;
            // 
            // fishSuitablePondLB
            // 
            this.fishSuitablePondLB.AutoSize = true;
            this.fishSuitablePondLB.Location = new System.Drawing.Point(47, 220);
            this.fishSuitablePondLB.Name = "fishSuitablePondLB";
            this.fishSuitablePondLB.Size = new System.Drawing.Size(208, 13);
            this.fishSuitablePondLB.TabIndex = 6;
            this.fishSuitablePondLB.Text = "Suitable fish types are Golfish, Orfe, Tench";
            // 
            // recFoodPondLB
            // 
            this.recFoodPondLB.AutoSize = true;
            this.recFoodPondLB.Location = new System.Drawing.Point(47, 183);
            this.recFoodPondLB.Name = "recFoodPondLB";
            this.recFoodPondLB.Size = new System.Drawing.Size(212, 13);
            this.recFoodPondLB.TabIndex = 4;
            this.recFoodPondLB.Text = "recommended food type for this time of year";
            // 
            // linerSizePondLB
            // 
            this.linerSizePondLB.AutoSize = true;
            this.linerSizePondLB.Location = new System.Drawing.Point(47, 142);
            this.linerSizePondLB.Name = "linerSizePondLB";
            this.linerSizePondLB.Size = new System.Drawing.Size(88, 13);
            this.linerSizePondLB.TabIndex = 3;
            this.linerSizePondLB.Text = "liner size required";
            // 
            // uvRequiredPondLB
            // 
            this.uvRequiredPondLB.AutoSize = true;
            this.uvRequiredPondLB.Location = new System.Drawing.Point(47, 104);
            this.uvRequiredPondLB.Name = "uvRequiredPondLB";
            this.uvRequiredPondLB.Size = new System.Drawing.Size(130, 13);
            this.uvRequiredPondLB.TabIndex = 2;
            this.uvRequiredPondLB.Text = "recommended uv strength";
            // 
            // turnoverRatePondLB
            // 
            this.turnoverRatePondLB.AutoSize = true;
            this.turnoverRatePondLB.Location = new System.Drawing.Point(47, 66);
            this.turnoverRatePondLB.Name = "turnoverRatePondLB";
            this.turnoverRatePondLB.Size = new System.Drawing.Size(189, 13);
            this.turnoverRatePondLB.TabIndex = 1;
            this.turnoverRatePondLB.Text = "recommended turnover rate (gph / lph)";
            // 
            // volumeOverviewPondLB
            // 
            this.volumeOverviewPondLB.AutoSize = true;
            this.volumeOverviewPondLB.Location = new System.Drawing.Point(47, 28);
            this.volumeOverviewPondLB.Name = "volumeOverviewPondLB";
            this.volumeOverviewPondLB.Size = new System.Drawing.Size(68, 13);
            this.volumeOverviewPondLB.TabIndex = 0;
            this.volumeOverviewPondLB.Text = "pond volume";
            // 
            // dimensionsPondGB
            // 
            this.dimensionsPondGB.Controls.Add(this.dimensionsPondBT);
            this.dimensionsPondGB.Controls.Add(this.depthPondLB);
            this.dimensionsPondGB.Controls.Add(this.lengthPondTB);
            this.dimensionsPondGB.Controls.Add(this.widthPondLB);
            this.dimensionsPondGB.Controls.Add(this.widthPondTB);
            this.dimensionsPondGB.Controls.Add(this.lengthPondLB);
            this.dimensionsPondGB.Controls.Add(this.depthPondTB);
            this.dimensionsPondGB.Location = new System.Drawing.Point(5, 4);
            this.dimensionsPondGB.Name = "dimensionsPondGB";
            this.dimensionsPondGB.Size = new System.Drawing.Size(158, 220);
            this.dimensionsPondGB.TabIndex = 30;
            this.dimensionsPondGB.TabStop = false;
            this.dimensionsPondGB.Text = "Dimensions";
            // 
            // dimensionsPondBT
            // 
            this.dimensionsPondBT.Location = new System.Drawing.Point(40, 183);
            this.dimensionsPondBT.Name = "dimensionsPondBT";
            this.dimensionsPondBT.Size = new System.Drawing.Size(60, 23);
            this.dimensionsPondBT.TabIndex = 3;
            this.dimensionsPondBT.Text = "Submit";
            this.dimensionsPondBT.UseVisualStyleBackColor = true;
            this.dimensionsPondBT.Click += new System.EventHandler(this.dimensionsPondBT_Click);
            // 
            // depthPondLB
            // 
            this.depthPondLB.AutoSize = true;
            this.depthPondLB.Location = new System.Drawing.Point(22, 135);
            this.depthPondLB.Name = "depthPondLB";
            this.depthPondLB.Size = new System.Drawing.Size(36, 13);
            this.depthPondLB.TabIndex = 29;
            this.depthPondLB.Text = "Depth";
            // 
            // lengthPondTB
            // 
            this.lengthPondTB.Location = new System.Drawing.Point(25, 46);
            this.lengthPondTB.Name = "lengthPondTB";
            this.lengthPondTB.Size = new System.Drawing.Size(100, 20);
            this.lengthPondTB.TabIndex = 0;
            this.lengthPondTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NewDimensions_KeyDown);
            // 
            // widthPondLB
            // 
            this.widthPondLB.AutoSize = true;
            this.widthPondLB.Location = new System.Drawing.Point(22, 82);
            this.widthPondLB.Name = "widthPondLB";
            this.widthPondLB.Size = new System.Drawing.Size(35, 13);
            this.widthPondLB.TabIndex = 28;
            this.widthPondLB.Text = "Width";
            // 
            // widthPondTB
            // 
            this.widthPondTB.Location = new System.Drawing.Point(25, 98);
            this.widthPondTB.Name = "widthPondTB";
            this.widthPondTB.Size = new System.Drawing.Size(100, 20);
            this.widthPondTB.TabIndex = 1;
            this.widthPondTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NewDimensions_KeyDown);
            // 
            // lengthPondLB
            // 
            this.lengthPondLB.AutoSize = true;
            this.lengthPondLB.Location = new System.Drawing.Point(22, 27);
            this.lengthPondLB.Name = "lengthPondLB";
            this.lengthPondLB.Size = new System.Drawing.Size(40, 13);
            this.lengthPondLB.TabIndex = 27;
            this.lengthPondLB.Text = "Length";
            // 
            // depthPondTB
            // 
            this.depthPondTB.Location = new System.Drawing.Point(25, 151);
            this.depthPondTB.Name = "depthPondTB";
            this.depthPondTB.Size = new System.Drawing.Size(100, 20);
            this.depthPondTB.TabIndex = 2;
            this.depthPondTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NewDimensions_KeyDown);
            // 
            // fishTP
            // 
            this.fishTP.Controls.Add(this.addLivestockFishGB);
            this.fishTP.Controls.Add(this.tableFishDGV);
            this.fishTP.Location = new System.Drawing.Point(4, 22);
            this.fishTP.Name = "fishTP";
            this.fishTP.Padding = new System.Windows.Forms.Padding(3);
            this.fishTP.Size = new System.Drawing.Size(674, 487);
            this.fishTP.TabIndex = 0;
            this.fishTP.Text = "Fish";
            this.fishTP.UseVisualStyleBackColor = true;
            // 
            // addLivestockFishGB
            // 
            this.addLivestockFishGB.Controls.Add(this.sizeFishTB);
            this.addLivestockFishGB.Controls.Add(this.sizeFishLB);
            this.addLivestockFishGB.Controls.Add(this.genderFishGB);
            this.addLivestockFishGB.Controls.Add(this.fishCB);
            this.addLivestockFishGB.Controls.Add(this.ageFishTB);
            this.addLivestockFishGB.Controls.Add(this.commentsFishLB);
            this.addLivestockFishGB.Controls.Add(this.addFishBT);
            this.addLivestockFishGB.Controls.Add(this.commentsFishTB);
            this.addLivestockFishGB.Controls.Add(this.removeFishBT);
            this.addLivestockFishGB.Controls.Add(this.ageFishLB);
            this.addLivestockFishGB.Controls.Add(this.speciesLB);
            this.addLivestockFishGB.Location = new System.Drawing.Point(5, 4);
            this.addLivestockFishGB.Name = "addLivestockFishGB";
            this.addLivestockFishGB.Size = new System.Drawing.Size(191, 480);
            this.addLivestockFishGB.TabIndex = 25;
            this.addLivestockFishGB.TabStop = false;
            this.addLivestockFishGB.Text = "Add Fish";
            // 
            // sizeFishTB
            // 
            this.sizeFishTB.Location = new System.Drawing.Point(16, 208);
            this.sizeFishTB.Name = "sizeFishTB";
            this.sizeFishTB.Size = new System.Drawing.Size(151, 20);
            this.sizeFishTB.TabIndex = 4;
            // 
            // sizeFishLB
            // 
            this.sizeFishLB.AutoSize = true;
            this.sizeFishLB.Location = new System.Drawing.Point(13, 192);
            this.sizeFishLB.Name = "sizeFishLB";
            this.sizeFishLB.Size = new System.Drawing.Size(27, 13);
            this.sizeFishLB.TabIndex = 31;
            this.sizeFishLB.Text = "Size";
            // 
            // genderFishGB
            // 
            this.genderFishGB.Controls.Add(this.unknownFishRB);
            this.genderFishGB.Controls.Add(this.femaleFishRB);
            this.genderFishGB.Controls.Add(this.maleFishRB);
            this.genderFishGB.Location = new System.Drawing.Point(16, 75);
            this.genderFishGB.Name = "genderFishGB";
            this.genderFishGB.Size = new System.Drawing.Size(151, 100);
            this.genderFishGB.TabIndex = 29;
            this.genderFishGB.TabStop = false;
            this.genderFishGB.Text = "Gender";
            // 
            // unknownFishRB
            // 
            this.unknownFishRB.AutoSize = true;
            this.unknownFishRB.Location = new System.Drawing.Point(8, 72);
            this.unknownFishRB.Name = "unknownFishRB";
            this.unknownFishRB.Size = new System.Drawing.Size(71, 17);
            this.unknownFishRB.TabIndex = 3;
            this.unknownFishRB.TabStop = true;
            this.unknownFishRB.Text = "Unknown";
            this.unknownFishRB.UseVisualStyleBackColor = true;
            // 
            // femaleFishRB
            // 
            this.femaleFishRB.AutoSize = true;
            this.femaleFishRB.Location = new System.Drawing.Point(8, 46);
            this.femaleFishRB.Name = "femaleFishRB";
            this.femaleFishRB.Size = new System.Drawing.Size(59, 17);
            this.femaleFishRB.TabIndex = 2;
            this.femaleFishRB.TabStop = true;
            this.femaleFishRB.Text = "Female";
            this.femaleFishRB.UseVisualStyleBackColor = true;
            // 
            // maleFishRB
            // 
            this.maleFishRB.AutoSize = true;
            this.maleFishRB.Location = new System.Drawing.Point(8, 20);
            this.maleFishRB.Name = "maleFishRB";
            this.maleFishRB.Size = new System.Drawing.Size(48, 17);
            this.maleFishRB.TabIndex = 1;
            this.maleFishRB.TabStop = true;
            this.maleFishRB.Text = "Male";
            this.maleFishRB.UseVisualStyleBackColor = true;
            // 
            // fishCB
            // 
            this.fishCB.FormattingEnabled = true;
            this.fishCB.Location = new System.Drawing.Point(17, 37);
            this.fishCB.Name = "fishCB";
            this.fishCB.Size = new System.Drawing.Size(150, 21);
            this.fishCB.TabIndex = 0;
            // 
            // ageFishTB
            // 
            this.ageFishTB.Location = new System.Drawing.Point(16, 250);
            this.ageFishTB.Name = "ageFishTB";
            this.ageFishTB.Size = new System.Drawing.Size(151, 20);
            this.ageFishTB.TabIndex = 5;
            // 
            // commentsFishLB
            // 
            this.commentsFishLB.AutoSize = true;
            this.commentsFishLB.Location = new System.Drawing.Point(23, 284);
            this.commentsFishLB.Name = "commentsFishLB";
            this.commentsFishLB.Size = new System.Drawing.Size(56, 13);
            this.commentsFishLB.TabIndex = 24;
            this.commentsFishLB.Text = "Comments";
            // 
            // addFishBT
            // 
            this.addFishBT.Location = new System.Drawing.Point(107, 407);
            this.addFishBT.Name = "addFishBT";
            this.addFishBT.Size = new System.Drawing.Size(60, 23);
            this.addFishBT.TabIndex = 8;
            this.addFishBT.Text = "Add";
            this.addFishBT.UseVisualStyleBackColor = true;
            this.addFishBT.Click += new System.EventHandler(this.AddFishBT_Click);
            // 
            // commentsFishTB
            // 
            this.commentsFishTB.Location = new System.Drawing.Point(16, 300);
            this.commentsFishTB.Multiline = true;
            this.commentsFishTB.Name = "commentsFishTB";
            this.commentsFishTB.Size = new System.Drawing.Size(146, 92);
            this.commentsFishTB.TabIndex = 6;
            // 
            // removeFishBT
            // 
            this.removeFishBT.Location = new System.Drawing.Point(19, 407);
            this.removeFishBT.Name = "removeFishBT";
            this.removeFishBT.Size = new System.Drawing.Size(60, 23);
            this.removeFishBT.TabIndex = 7;
            this.removeFishBT.Text = "Remove Record";
            this.removeFishBT.UseVisualStyleBackColor = true;
            this.removeFishBT.Click += new System.EventHandler(this.RemoveFishBT_Click);
            // 
            // ageFishLB
            // 
            this.ageFishLB.AutoSize = true;
            this.ageFishLB.Location = new System.Drawing.Point(13, 234);
            this.ageFishLB.Name = "ageFishLB";
            this.ageFishLB.Size = new System.Drawing.Size(26, 13);
            this.ageFishLB.TabIndex = 9;
            this.ageFishLB.Text = "Age";
            // 
            // speciesLB
            // 
            this.speciesLB.AutoSize = true;
            this.speciesLB.Location = new System.Drawing.Point(14, 21);
            this.speciesLB.Name = "speciesLB";
            this.speciesLB.Size = new System.Drawing.Size(45, 13);
            this.speciesLB.TabIndex = 6;
            this.speciesLB.Text = "Species";
            // 
            // tableFishDGV
            // 
            this.tableFishDGV.AllowUserToAddRows = false;
            this.tableFishDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableFishDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableFishDGV.Location = new System.Drawing.Point(202, 3);
            this.tableFishDGV.Name = "tableFishDGV";
            this.tableFishDGV.ReadOnly = true;
            this.tableFishDGV.RowHeadersVisible = false;
            this.tableFishDGV.Size = new System.Drawing.Size(469, 481);
            this.tableFishDGV.TabIndex = 11;
            // 
            // parametersTP
            // 
            this.parametersTP.Controls.Add(this.addRecordParametersGB);
            this.parametersTP.Controls.Add(this.tableParametersDGV);
            this.parametersTP.Location = new System.Drawing.Point(4, 22);
            this.parametersTP.Name = "parametersTP";
            this.parametersTP.Size = new System.Drawing.Size(674, 487);
            this.parametersTP.TabIndex = 2;
            this.parametersTP.Text = "Water Chemistry";
            this.parametersTP.UseVisualStyleBackColor = true;
            // 
            // addRecordParametersGB
            // 
            this.addRecordParametersGB.Controls.Add(this.csvWaterChemistryBT);
            this.addRecordParametersGB.Controls.Add(this.temperatureParametersTB);
            this.addRecordParametersGB.Controls.Add(this.temperatureParametersLB);
            this.addRecordParametersGB.Controls.Add(this.ghParametersTB);
            this.addRecordParametersGB.Controls.Add(this.removeRecordParametersBT);
            this.addRecordParametersGB.Controls.Add(this.addRecordParametersBT);
            this.addRecordParametersGB.Controls.Add(this.ghParametersLB);
            this.addRecordParametersGB.Controls.Add(this.ammoniaParametersTB);
            this.addRecordParametersGB.Controls.Add(this.khParametersLB);
            this.addRecordParametersGB.Controls.Add(this.nitriteParametersTB);
            this.addRecordParametersGB.Controls.Add(this.nitrateParametersLB);
            this.addRecordParametersGB.Controls.Add(this.nitrateParametersTB);
            this.addRecordParametersGB.Controls.Add(this.nitriteParametersLB);
            this.addRecordParametersGB.Controls.Add(this.phParametersTB);
            this.addRecordParametersGB.Controls.Add(this.ammoniaParametersLB);
            this.addRecordParametersGB.Controls.Add(this.khParametersTB);
            this.addRecordParametersGB.Controls.Add(this.phParametersLB);
            this.addRecordParametersGB.Controls.Add(this.dateParametersDTP);
            this.addRecordParametersGB.Controls.Add(this.dateParametersLB);
            this.addRecordParametersGB.Location = new System.Drawing.Point(5, 4);
            this.addRecordParametersGB.Name = "addRecordParametersGB";
            this.addRecordParametersGB.Size = new System.Drawing.Size(191, 480);
            this.addRecordParametersGB.TabIndex = 24;
            this.addRecordParametersGB.TabStop = false;
            this.addRecordParametersGB.Text = "Add Record";
            // 
            // csvWaterChemistryBT
            // 
            this.csvWaterChemistryBT.Location = new System.Drawing.Point(18, 443);
            this.csvWaterChemistryBT.Name = "csvWaterChemistryBT";
            this.csvWaterChemistryBT.Size = new System.Drawing.Size(126, 23);
            this.csvWaterChemistryBT.TabIndex = 26;
            this.csvWaterChemistryBT.Text = "Export to CSV file";
            this.csvWaterChemistryBT.UseVisualStyleBackColor = true;
            this.csvWaterChemistryBT.Click += new System.EventHandler(this.csvWaterChemistryBT_Click);
            // 
            // temperatureParametersTB
            // 
            this.temperatureParametersTB.Location = new System.Drawing.Point(18, 84);
            this.temperatureParametersTB.Name = "temperatureParametersTB";
            this.temperatureParametersTB.Size = new System.Drawing.Size(125, 20);
            this.temperatureParametersTB.TabIndex = 1;
            // 
            // temperatureParametersLB
            // 
            this.temperatureParametersLB.AutoSize = true;
            this.temperatureParametersLB.Location = new System.Drawing.Point(15, 68);
            this.temperatureParametersLB.Name = "temperatureParametersLB";
            this.temperatureParametersLB.Size = new System.Drawing.Size(67, 13);
            this.temperatureParametersLB.TabIndex = 25;
            this.temperatureParametersLB.Text = "Temperature";
            // 
            // ghParametersTB
            // 
            this.ghParametersTB.Location = new System.Drawing.Point(18, 329);
            this.ghParametersTB.Name = "ghParametersTB";
            this.ghParametersTB.Size = new System.Drawing.Size(125, 20);
            this.ghParametersTB.TabIndex = 6;
            // 
            // removeRecordParametersBT
            // 
            this.removeRecordParametersBT.Location = new System.Drawing.Point(17, 411);
            this.removeRecordParametersBT.Name = "removeRecordParametersBT";
            this.removeRecordParametersBT.Size = new System.Drawing.Size(60, 23);
            this.removeRecordParametersBT.TabIndex = 8;
            this.removeRecordParametersBT.Text = "Remove Record";
            this.removeRecordParametersBT.UseVisualStyleBackColor = true;
            this.removeRecordParametersBT.Click += new System.EventHandler(this.RemoveRecordBT_Click);
            // 
            // addRecordParametersBT
            // 
            this.addRecordParametersBT.Location = new System.Drawing.Point(84, 411);
            this.addRecordParametersBT.Name = "addRecordParametersBT";
            this.addRecordParametersBT.Size = new System.Drawing.Size(60, 23);
            this.addRecordParametersBT.TabIndex = 9;
            this.addRecordParametersBT.Text = "Add";
            this.addRecordParametersBT.UseVisualStyleBackColor = true;
            this.addRecordParametersBT.Click += new System.EventHandler(this.AddRecord_Click);
            // 
            // ghParametersLB
            // 
            this.ghParametersLB.AutoSize = true;
            this.ghParametersLB.Location = new System.Drawing.Point(15, 313);
            this.ghParametersLB.Name = "ghParametersLB";
            this.ghParametersLB.Size = new System.Drawing.Size(52, 13);
            this.ghParametersLB.TabIndex = 20;
            this.ghParametersLB.Text = "GH (ppm)";
            // 
            // ammoniaParametersTB
            // 
            this.ammoniaParametersTB.Location = new System.Drawing.Point(18, 180);
            this.ammoniaParametersTB.Name = "ammoniaParametersTB";
            this.ammoniaParametersTB.Size = new System.Drawing.Size(125, 20);
            this.ammoniaParametersTB.TabIndex = 3;
            // 
            // khParametersLB
            // 
            this.khParametersLB.AutoSize = true;
            this.khParametersLB.Location = new System.Drawing.Point(15, 359);
            this.khParametersLB.Name = "khParametersLB";
            this.khParametersLB.Size = new System.Drawing.Size(51, 13);
            this.khParametersLB.TabIndex = 19;
            this.khParametersLB.Text = "KH (ppm)";
            // 
            // nitriteParametersTB
            // 
            this.nitriteParametersTB.Location = new System.Drawing.Point(18, 229);
            this.nitriteParametersTB.Name = "nitriteParametersTB";
            this.nitriteParametersTB.Size = new System.Drawing.Size(125, 20);
            this.nitriteParametersTB.TabIndex = 4;
            // 
            // nitrateParametersLB
            // 
            this.nitrateParametersLB.AutoSize = true;
            this.nitrateParametersLB.Location = new System.Drawing.Point(15, 264);
            this.nitrateParametersLB.Name = "nitrateParametersLB";
            this.nitrateParametersLB.Size = new System.Drawing.Size(67, 13);
            this.nitrateParametersLB.TabIndex = 18;
            this.nitrateParametersLB.Text = "Nitrate (NO₃)";
            // 
            // nitrateParametersTB
            // 
            this.nitrateParametersTB.Location = new System.Drawing.Point(18, 280);
            this.nitrateParametersTB.Name = "nitrateParametersTB";
            this.nitrateParametersTB.Size = new System.Drawing.Size(125, 20);
            this.nitrateParametersTB.TabIndex = 5;
            // 
            // nitriteParametersLB
            // 
            this.nitriteParametersLB.AutoSize = true;
            this.nitriteParametersLB.Location = new System.Drawing.Point(15, 213);
            this.nitriteParametersLB.Name = "nitriteParametersLB";
            this.nitriteParametersLB.Size = new System.Drawing.Size(63, 13);
            this.nitriteParametersLB.TabIndex = 17;
            this.nitriteParametersLB.Text = "Nitrite (NO₂)";
            // 
            // phParametersTB
            // 
            this.phParametersTB.Location = new System.Drawing.Point(18, 131);
            this.phParametersTB.Name = "phParametersTB";
            this.phParametersTB.Size = new System.Drawing.Size(125, 20);
            this.phParametersTB.TabIndex = 2;
            // 
            // ammoniaParametersLB
            // 
            this.ammoniaParametersLB.AutoSize = true;
            this.ammoniaParametersLB.Location = new System.Drawing.Point(15, 164);
            this.ammoniaParametersLB.Name = "ammoniaParametersLB";
            this.ammoniaParametersLB.Size = new System.Drawing.Size(79, 13);
            this.ammoniaParametersLB.TabIndex = 16;
            this.ammoniaParametersLB.Text = "Ammonia (NH₃)";
            // 
            // khParametersTB
            // 
            this.khParametersTB.Location = new System.Drawing.Point(18, 375);
            this.khParametersTB.Name = "khParametersTB";
            this.khParametersTB.Size = new System.Drawing.Size(125, 20);
            this.khParametersTB.TabIndex = 7;
            // 
            // phParametersLB
            // 
            this.phParametersLB.AutoSize = true;
            this.phParametersLB.Location = new System.Drawing.Point(15, 115);
            this.phParametersLB.Name = "phParametersLB";
            this.phParametersLB.Size = new System.Drawing.Size(21, 13);
            this.phParametersLB.TabIndex = 15;
            this.phParametersLB.Text = "pH";
            // 
            // dateParametersDTP
            // 
            this.dateParametersDTP.Location = new System.Drawing.Point(18, 36);
            this.dateParametersDTP.Name = "dateParametersDTP";
            this.dateParametersDTP.Size = new System.Drawing.Size(125, 20);
            this.dateParametersDTP.TabIndex = 0;
            // 
            // dateParametersLB
            // 
            this.dateParametersLB.AutoSize = true;
            this.dateParametersLB.Location = new System.Drawing.Point(15, 20);
            this.dateParametersLB.Name = "dateParametersLB";
            this.dateParametersLB.Size = new System.Drawing.Size(30, 13);
            this.dateParametersLB.TabIndex = 14;
            this.dateParametersLB.Text = "Date";
            // 
            // tableParametersDGV
            // 
            this.tableParametersDGV.AllowUserToAddRows = false;
            this.tableParametersDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableParametersDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableParametersDGV.Location = new System.Drawing.Point(202, 3);
            this.tableParametersDGV.Name = "tableParametersDGV";
            this.tableParametersDGV.ReadOnly = true;
            this.tableParametersDGV.RowHeadersVisible = false;
            this.tableParametersDGV.Size = new System.Drawing.Size(469, 481);
            this.tableParametersDGV.TabIndex = 10;
            this.tableParametersDGV.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.ParameterGrid_CellFormatting);
            // 
            // foodTP
            // 
            this.foodTP.Controls.Add(this.addFoodGB);
            this.foodTP.Controls.Add(this.tableFoodDGV);
            this.foodTP.Location = new System.Drawing.Point(4, 22);
            this.foodTP.Name = "foodTP";
            this.foodTP.Size = new System.Drawing.Size(674, 487);
            this.foodTP.TabIndex = 3;
            this.foodTP.Text = "Food";
            this.foodTP.UseVisualStyleBackColor = true;
            // 
            // addFoodGB
            // 
            this.addFoodGB.Controls.Add(this.sizeFoodGB);
            this.addFoodGB.Controls.Add(this.nameFoodLB);
            this.addFoodGB.Controls.Add(this.removeFoodBT);
            this.addFoodGB.Controls.Add(this.addFoodBT);
            this.addFoodGB.Controls.Add(this.typeFoodGB);
            this.addFoodGB.Controls.Add(this.commentsFoodTB);
            this.addFoodGB.Controls.Add(this.commentsFoodLB);
            this.addFoodGB.Controls.Add(this.nameFoodTB);
            this.addFoodGB.Location = new System.Drawing.Point(5, 4);
            this.addFoodGB.Name = "addFoodGB";
            this.addFoodGB.Size = new System.Drawing.Size(191, 480);
            this.addFoodGB.TabIndex = 40;
            this.addFoodGB.TabStop = false;
            this.addFoodGB.Text = "Add Food";
            // 
            // sizeFoodGB
            // 
            this.sizeFoodGB.Controls.Add(this.pelletFoodRB);
            this.sizeFoodGB.Controls.Add(this.flakeFoodRB);
            this.sizeFoodGB.Controls.Add(this.sticksFoodRB);
            this.sizeFoodGB.Location = new System.Drawing.Point(15, 78);
            this.sizeFoodGB.Name = "sizeFoodGB";
            this.sizeFoodGB.Size = new System.Drawing.Size(156, 99);
            this.sizeFoodGB.TabIndex = 37;
            this.sizeFoodGB.TabStop = false;
            this.sizeFoodGB.Text = "Size";
            // 
            // pelletFoodRB
            // 
            this.pelletFoodRB.AutoSize = true;
            this.pelletFoodRB.Location = new System.Drawing.Point(6, 47);
            this.pelletFoodRB.Name = "pelletFoodRB";
            this.pelletFoodRB.Size = new System.Drawing.Size(51, 17);
            this.pelletFoodRB.TabIndex = 2;
            this.pelletFoodRB.TabStop = true;
            this.pelletFoodRB.Text = "Pellet";
            this.pelletFoodRB.UseVisualStyleBackColor = true;
            this.pelletFoodRB.CheckedChanged += new System.EventHandler(this.FoodTypeBT_CheckedChanged);
            // 
            // flakeFoodRB
            // 
            this.flakeFoodRB.AutoSize = true;
            this.flakeFoodRB.Location = new System.Drawing.Point(6, 24);
            this.flakeFoodRB.Name = "flakeFoodRB";
            this.flakeFoodRB.Size = new System.Drawing.Size(51, 17);
            this.flakeFoodRB.TabIndex = 1;
            this.flakeFoodRB.TabStop = true;
            this.flakeFoodRB.Text = "Flake";
            this.flakeFoodRB.UseVisualStyleBackColor = true;
            this.flakeFoodRB.CheckedChanged += new System.EventHandler(this.FoodTypeBT_CheckedChanged);
            // 
            // sticksFoodRB
            // 
            this.sticksFoodRB.AutoSize = true;
            this.sticksFoodRB.Location = new System.Drawing.Point(6, 70);
            this.sticksFoodRB.Name = "sticksFoodRB";
            this.sticksFoodRB.Size = new System.Drawing.Size(54, 17);
            this.sticksFoodRB.TabIndex = 3;
            this.sticksFoodRB.TabStop = true;
            this.sticksFoodRB.Text = "Sticks";
            this.sticksFoodRB.UseVisualStyleBackColor = true;
            this.sticksFoodRB.CheckedChanged += new System.EventHandler(this.FoodTypeBT_CheckedChanged);
            // 
            // nameFoodLB
            // 
            this.nameFoodLB.AutoSize = true;
            this.nameFoodLB.Location = new System.Drawing.Point(15, 24);
            this.nameFoodLB.Name = "nameFoodLB";
            this.nameFoodLB.Size = new System.Drawing.Size(35, 13);
            this.nameFoodLB.TabIndex = 25;
            this.nameFoodLB.Text = "Name";
            // 
            // removeFoodBT
            // 
            this.removeFoodBT.Location = new System.Drawing.Point(14, 425);
            this.removeFoodBT.Name = "removeFoodBT";
            this.removeFoodBT.Size = new System.Drawing.Size(60, 23);
            this.removeFoodBT.TabIndex = 8;
            this.removeFoodBT.Text = "Remove Record";
            this.removeFoodBT.UseVisualStyleBackColor = true;
            this.removeFoodBT.Click += new System.EventHandler(this.RemoveFoodBT_Click);
            // 
            // addFoodBT
            // 
            this.addFoodBT.Location = new System.Drawing.Point(112, 425);
            this.addFoodBT.Name = "addFoodBT";
            this.addFoodBT.Size = new System.Drawing.Size(60, 23);
            this.addFoodBT.TabIndex = 9;
            this.addFoodBT.Text = "Add Food";
            this.addFoodBT.UseVisualStyleBackColor = true;
            this.addFoodBT.Click += new System.EventHandler(this.AddFoodBT_Click);
            // 
            // typeFoodGB
            // 
            this.typeFoodGB.Controls.Add(this.wheatgermFoodRB);
            this.typeFoodGB.Controls.Add(this.stapleFoodRB);
            this.typeFoodGB.Controls.Add(this.proteinFoodRB);
            this.typeFoodGB.Location = new System.Drawing.Point(15, 183);
            this.typeFoodGB.Name = "typeFoodGB";
            this.typeFoodGB.Size = new System.Drawing.Size(156, 99);
            this.typeFoodGB.TabIndex = 38;
            this.typeFoodGB.TabStop = false;
            this.typeFoodGB.Text = "Type";
            // 
            // wheatgermFoodRB
            // 
            this.wheatgermFoodRB.AutoSize = true;
            this.wheatgermFoodRB.Location = new System.Drawing.Point(6, 19);
            this.wheatgermFoodRB.Name = "wheatgermFoodRB";
            this.wheatgermFoodRB.Size = new System.Drawing.Size(80, 17);
            this.wheatgermFoodRB.TabIndex = 4;
            this.wheatgermFoodRB.TabStop = true;
            this.wheatgermFoodRB.Text = "Wheatgerm";
            this.wheatgermFoodRB.UseVisualStyleBackColor = true;
            this.wheatgermFoodRB.CheckedChanged += new System.EventHandler(this.FoodTypeBT_CheckedChanged);
            // 
            // stapleFoodRB
            // 
            this.stapleFoodRB.AutoSize = true;
            this.stapleFoodRB.Location = new System.Drawing.Point(6, 42);
            this.stapleFoodRB.Name = "stapleFoodRB";
            this.stapleFoodRB.Size = new System.Drawing.Size(55, 17);
            this.stapleFoodRB.TabIndex = 5;
            this.stapleFoodRB.TabStop = true;
            this.stapleFoodRB.Text = "Staple";
            this.stapleFoodRB.UseVisualStyleBackColor = true;
            this.stapleFoodRB.CheckedChanged += new System.EventHandler(this.FoodTypeBT_CheckedChanged);
            // 
            // proteinFoodRB
            // 
            this.proteinFoodRB.AutoSize = true;
            this.proteinFoodRB.Location = new System.Drawing.Point(6, 65);
            this.proteinFoodRB.Name = "proteinFoodRB";
            this.proteinFoodRB.Size = new System.Drawing.Size(120, 17);
            this.proteinFoodRB.TabIndex = 6;
            this.proteinFoodRB.TabStop = true;
            this.proteinFoodRB.Text = "High Protein Growth";
            this.proteinFoodRB.UseVisualStyleBackColor = true;
            this.proteinFoodRB.CheckedChanged += new System.EventHandler(this.FoodTypeBT_CheckedChanged);
            // 
            // commentsFoodTB
            // 
            this.commentsFoodTB.Location = new System.Drawing.Point(15, 310);
            this.commentsFoodTB.Multiline = true;
            this.commentsFoodTB.Name = "commentsFoodTB";
            this.commentsFoodTB.Size = new System.Drawing.Size(156, 99);
            this.commentsFoodTB.TabIndex = 7;
            // 
            // commentsFoodLB
            // 
            this.commentsFoodLB.AutoSize = true;
            this.commentsFoodLB.Location = new System.Drawing.Point(15, 294);
            this.commentsFoodLB.Name = "commentsFoodLB";
            this.commentsFoodLB.Size = new System.Drawing.Size(56, 13);
            this.commentsFoodLB.TabIndex = 28;
            this.commentsFoodLB.Text = "Comments";
            // 
            // nameFoodTB
            // 
            this.nameFoodTB.Location = new System.Drawing.Point(18, 40);
            this.nameFoodTB.Name = "nameFoodTB";
            this.nameFoodTB.Size = new System.Drawing.Size(153, 20);
            this.nameFoodTB.TabIndex = 0;
            // 
            // tableFoodDGV
            // 
            this.tableFoodDGV.AllowUserToAddRows = false;
            this.tableFoodDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableFoodDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableFoodDGV.Location = new System.Drawing.Point(202, 3);
            this.tableFoodDGV.Name = "tableFoodDGV";
            this.tableFoodDGV.ReadOnly = true;
            this.tableFoodDGV.RowHeadersVisible = false;
            this.tableFoodDGV.Size = new System.Drawing.Size(469, 481);
            this.tableFoodDGV.TabIndex = 10;
            // 
            // powerTP
            // 
            this.powerTP.Controls.Add(this.typeDeviceGB);
            this.powerTP.Controls.Add(this.loadDeviceBT);
            this.powerTP.Controls.Add(this.statusDeviceGB);
            this.powerTP.Controls.Add(this.scheduleDeviceGB);
            this.powerTP.Controls.Add(this.removeDeviceBT);
            this.powerTP.Controls.Add(this.addDeviceBT);
            this.powerTP.Controls.Add(this.currentDevicesLB);
            this.powerTP.Location = new System.Drawing.Point(4, 22);
            this.powerTP.Name = "powerTP";
            this.powerTP.Padding = new System.Windows.Forms.Padding(3);
            this.powerTP.Size = new System.Drawing.Size(674, 487);
            this.powerTP.TabIndex = 4;
            this.powerTP.Text = "Power";
            this.powerTP.UseVisualStyleBackColor = true;
            // 
            // typeDeviceGB
            // 
            this.typeDeviceGB.Controls.Add(this.typeDeviceLB);
            this.typeDeviceGB.Controls.Add(this.typeDeviceCB);
            this.typeDeviceGB.Controls.Add(this.nameDeviceLB);
            this.typeDeviceGB.Controls.Add(this.nameDeviceTB);
            this.typeDeviceGB.Location = new System.Drawing.Point(8, 3);
            this.typeDeviceGB.Name = "typeDeviceGB";
            this.typeDeviceGB.Size = new System.Drawing.Size(216, 141);
            this.typeDeviceGB.TabIndex = 11;
            this.typeDeviceGB.TabStop = false;
            this.typeDeviceGB.Text = "Device";
            // 
            // typeDeviceLB
            // 
            this.typeDeviceLB.AutoSize = true;
            this.typeDeviceLB.Location = new System.Drawing.Point(12, 78);
            this.typeDeviceLB.Name = "typeDeviceLB";
            this.typeDeviceLB.Size = new System.Drawing.Size(31, 13);
            this.typeDeviceLB.TabIndex = 12;
            this.typeDeviceLB.Text = "Type";
            // 
            // typeDeviceCB
            // 
            this.typeDeviceCB.FormattingEnabled = true;
            this.typeDeviceCB.Location = new System.Drawing.Point(15, 96);
            this.typeDeviceCB.Name = "typeDeviceCB";
            this.typeDeviceCB.Size = new System.Drawing.Size(182, 21);
            this.typeDeviceCB.TabIndex = 1;
            // 
            // nameDeviceLB
            // 
            this.nameDeviceLB.AutoSize = true;
            this.nameDeviceLB.Location = new System.Drawing.Point(12, 24);
            this.nameDeviceLB.Name = "nameDeviceLB";
            this.nameDeviceLB.Size = new System.Drawing.Size(35, 13);
            this.nameDeviceLB.TabIndex = 11;
            this.nameDeviceLB.Text = "Name";
            // 
            // nameDeviceTB
            // 
            this.nameDeviceTB.Location = new System.Drawing.Point(15, 42);
            this.nameDeviceTB.Name = "nameDeviceTB";
            this.nameDeviceTB.Size = new System.Drawing.Size(182, 20);
            this.nameDeviceTB.TabIndex = 0;
            // 
            // loadDeviceBT
            // 
            this.loadDeviceBT.Location = new System.Drawing.Point(86, 350);
            this.loadDeviceBT.Name = "loadDeviceBT";
            this.loadDeviceBT.Size = new System.Drawing.Size(60, 23);
            this.loadDeviceBT.TabIndex = 10;
            this.loadDeviceBT.Text = "Load";
            this.loadDeviceBT.UseVisualStyleBackColor = true;
            this.loadDeviceBT.Click += new System.EventHandler(this.LoadDeviceBT_Click);
            // 
            // statusDeviceGB
            // 
            this.statusDeviceGB.Controls.Add(this.scheduledDeviceChB);
            this.statusDeviceGB.Controls.Add(this.onDeviceRB);
            this.statusDeviceGB.Controls.Add(this.offDeviceRB);
            this.statusDeviceGB.Location = new System.Drawing.Point(8, 150);
            this.statusDeviceGB.Name = "statusDeviceGB";
            this.statusDeviceGB.Size = new System.Drawing.Size(216, 65);
            this.statusDeviceGB.TabIndex = 10;
            this.statusDeviceGB.TabStop = false;
            this.statusDeviceGB.Text = "Status";
            // 
            // scheduledDeviceChB
            // 
            this.scheduledDeviceChB.AutoSize = true;
            this.scheduledDeviceChB.Location = new System.Drawing.Point(130, 29);
            this.scheduledDeviceChB.Name = "scheduledDeviceChB";
            this.scheduledDeviceChB.Size = new System.Drawing.Size(77, 17);
            this.scheduledDeviceChB.TabIndex = 4;
            this.scheduledDeviceChB.Text = "Scheduled";
            this.scheduledDeviceChB.UseVisualStyleBackColor = true;
            this.scheduledDeviceChB.CheckedChanged += new System.EventHandler(this.ScheduledCB_CheckedChanged);
            // 
            // onDeviceRB
            // 
            this.onDeviceRB.AutoSize = true;
            this.onDeviceRB.Location = new System.Drawing.Point(72, 28);
            this.onDeviceRB.Name = "onDeviceRB";
            this.onDeviceRB.Size = new System.Drawing.Size(39, 17);
            this.onDeviceRB.TabIndex = 3;
            this.onDeviceRB.TabStop = true;
            this.onDeviceRB.Text = "On";
            this.onDeviceRB.UseVisualStyleBackColor = true;
            // 
            // offDeviceRB
            // 
            this.offDeviceRB.AutoSize = true;
            this.offDeviceRB.Location = new System.Drawing.Point(10, 28);
            this.offDeviceRB.Name = "offDeviceRB";
            this.offDeviceRB.Size = new System.Drawing.Size(39, 17);
            this.offDeviceRB.TabIndex = 2;
            this.offDeviceRB.TabStop = true;
            this.offDeviceRB.Text = "Off";
            this.offDeviceRB.UseVisualStyleBackColor = true;
            // 
            // scheduleDeviceGB
            // 
            this.scheduleDeviceGB.Controls.Add(this.colon2LB);
            this.scheduleDeviceGB.Controls.Add(this.colonLB);
            this.scheduleDeviceGB.Controls.Add(this.finishDeviceLB);
            this.scheduleDeviceGB.Controls.Add(this.startDeviceLB);
            this.scheduleDeviceGB.Controls.Add(this.minDeviceLB);
            this.scheduleDeviceGB.Controls.Add(this.hourDeviceLB);
            this.scheduleDeviceGB.Controls.Add(this.finishMinDeviceTB);
            this.scheduleDeviceGB.Controls.Add(this.startMinDeviceTB);
            this.scheduleDeviceGB.Controls.Add(this.finishHrDeviceTB);
            this.scheduleDeviceGB.Controls.Add(this.startHrDeviceTB);
            this.scheduleDeviceGB.Location = new System.Drawing.Point(8, 221);
            this.scheduleDeviceGB.Name = "scheduleDeviceGB";
            this.scheduleDeviceGB.Size = new System.Drawing.Size(216, 117);
            this.scheduleDeviceGB.TabIndex = 9;
            this.scheduleDeviceGB.TabStop = false;
            this.scheduleDeviceGB.Text = "Schedule";
            // 
            // colon2LB
            // 
            this.colon2LB.AutoSize = true;
            this.colon2LB.Location = new System.Drawing.Point(103, 77);
            this.colon2LB.Name = "colon2LB";
            this.colon2LB.Size = new System.Drawing.Size(10, 13);
            this.colon2LB.TabIndex = 15;
            this.colon2LB.Text = ":";
            // 
            // colonLB
            // 
            this.colonLB.AutoSize = true;
            this.colonLB.Location = new System.Drawing.Point(103, 48);
            this.colonLB.Name = "colonLB";
            this.colonLB.Size = new System.Drawing.Size(10, 13);
            this.colonLB.TabIndex = 14;
            this.colonLB.Text = ":";
            // 
            // finishDeviceLB
            // 
            this.finishDeviceLB.AutoSize = true;
            this.finishDeviceLB.Location = new System.Drawing.Point(14, 80);
            this.finishDeviceLB.Name = "finishDeviceLB";
            this.finishDeviceLB.Size = new System.Drawing.Size(34, 13);
            this.finishDeviceLB.TabIndex = 13;
            this.finishDeviceLB.Text = "Finish";
            // 
            // startDeviceLB
            // 
            this.startDeviceLB.AutoSize = true;
            this.startDeviceLB.Location = new System.Drawing.Point(19, 52);
            this.startDeviceLB.Name = "startDeviceLB";
            this.startDeviceLB.Size = new System.Drawing.Size(29, 13);
            this.startDeviceLB.TabIndex = 12;
            this.startDeviceLB.Text = "Start";
            // 
            // minDeviceLB
            // 
            this.minDeviceLB.AutoSize = true;
            this.minDeviceLB.Location = new System.Drawing.Point(121, 25);
            this.minDeviceLB.Name = "minDeviceLB";
            this.minDeviceLB.Size = new System.Drawing.Size(39, 13);
            this.minDeviceLB.TabIndex = 11;
            this.minDeviceLB.Text = "Minute";
            // 
            // hourDeviceLB
            // 
            this.hourDeviceLB.AutoSize = true;
            this.hourDeviceLB.Location = new System.Drawing.Point(54, 25);
            this.hourDeviceLB.Name = "hourDeviceLB";
            this.hourDeviceLB.Size = new System.Drawing.Size(30, 13);
            this.hourDeviceLB.TabIndex = 8;
            this.hourDeviceLB.Text = "Hour";
            // 
            // finishMinDeviceTB
            // 
            this.finishMinDeviceTB.Location = new System.Drawing.Point(119, 73);
            this.finishMinDeviceTB.Name = "finishMinDeviceTB";
            this.finishMinDeviceTB.Size = new System.Drawing.Size(41, 20);
            this.finishMinDeviceTB.TabIndex = 8;
            // 
            // startMinDeviceTB
            // 
            this.startMinDeviceTB.Location = new System.Drawing.Point(119, 45);
            this.startMinDeviceTB.Name = "startMinDeviceTB";
            this.startMinDeviceTB.Size = new System.Drawing.Size(41, 20);
            this.startMinDeviceTB.TabIndex = 6;
            // 
            // finishHrDeviceTB
            // 
            this.finishHrDeviceTB.Location = new System.Drawing.Point(54, 73);
            this.finishHrDeviceTB.Name = "finishHrDeviceTB";
            this.finishHrDeviceTB.Size = new System.Drawing.Size(41, 20);
            this.finishHrDeviceTB.TabIndex = 7;
            // 
            // startHrDeviceTB
            // 
            this.startHrDeviceTB.Location = new System.Drawing.Point(54, 45);
            this.startHrDeviceTB.Name = "startHrDeviceTB";
            this.startHrDeviceTB.Size = new System.Drawing.Size(41, 20);
            this.startHrDeviceTB.TabIndex = 5;
            // 
            // removeDeviceBT
            // 
            this.removeDeviceBT.Location = new System.Drawing.Point(7, 350);
            this.removeDeviceBT.Name = "removeDeviceBT";
            this.removeDeviceBT.Size = new System.Drawing.Size(60, 23);
            this.removeDeviceBT.TabIndex = 9;
            this.removeDeviceBT.Text = "Remove";
            this.removeDeviceBT.UseVisualStyleBackColor = true;
            this.removeDeviceBT.Click += new System.EventHandler(this.RemoveDevicesBT_Click);
            // 
            // addDeviceBT
            // 
            this.addDeviceBT.Location = new System.Drawing.Point(165, 350);
            this.addDeviceBT.Name = "addDeviceBT";
            this.addDeviceBT.Size = new System.Drawing.Size(60, 23);
            this.addDeviceBT.TabIndex = 11;
            this.addDeviceBT.Text = "Add";
            this.addDeviceBT.UseVisualStyleBackColor = true;
            this.addDeviceBT.Click += new System.EventHandler(this.SubmitDeviceBT_Click);
            // 
            // currentDevicesLB
            // 
            this.currentDevicesLB.FormattingEnabled = true;
            this.currentDevicesLB.Location = new System.Drawing.Point(238, 8);
            this.currentDevicesLB.Name = "currentDevicesLB";
            this.currentDevicesLB.Size = new System.Drawing.Size(430, 472);
            this.currentDevicesLB.TabIndex = 12;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pictureBox17);
            this.groupBox2.Controls.Add(this.pictureBox18);
            this.groupBox2.Controls.Add(this.pictureBox19);
            this.groupBox2.Controls.Add(this.pictureBox20);
            this.groupBox2.Controls.Add(this.pictureBox21);
            this.groupBox2.Controls.Add(this.pictureBox22);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(2, 255);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(666, 251);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Technical Information";
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::PondManager.Properties.Resources.stocking;
            this.pictureBox17.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox17.Location = new System.Drawing.Point(9, 210);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(32, 32);
            this.pictureBox17.TabIndex = 21;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::PondManager.Properties.Resources.food;
            this.pictureBox18.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox18.Location = new System.Drawing.Point(9, 172);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(32, 32);
            this.pictureBox18.TabIndex = 20;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::PondManager.Properties.Resources.liner;
            this.pictureBox19.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox19.Location = new System.Drawing.Point(9, 132);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(32, 32);
            this.pictureBox19.TabIndex = 19;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::PondManager.Properties.Resources.uv;
            this.pictureBox20.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox20.Location = new System.Drawing.Point(9, 95);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(32, 32);
            this.pictureBox20.TabIndex = 18;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::PondManager.Properties.Resources.waterchange;
            this.pictureBox21.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox21.Location = new System.Drawing.Point(9, 57);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(32, 32);
            this.pictureBox21.TabIndex = 17;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::PondManager.Properties.Resources.volume;
            this.pictureBox22.InitialImage = global::PondManager.Properties.Resources.ammonia;
            this.pictureBox22.Location = new System.Drawing.Point(9, 19);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(32, 32);
            this.pictureBox22.TabIndex = 16;
            this.pictureBox22.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(208, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Suitable fish types are Golfish, Orfe, Tench";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(212, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "recommended food type for this time of year";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "liner size required";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(130, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "recommended uv strength";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(47, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(189, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "recommended turnover rate (gph / lph)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(47, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "pond volume";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 528);
            this.Controls.Add(this.navigationTC);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "Pond Manager";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.navigationTC.ResumeLayout(false);
            this.overviewTP.ResumeLayout(false);
            this.maintenanceOverviewGB.ResumeLayout(false);
            this.maintenanceOverviewGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waterTestOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterCleanOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pumpCleanOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uvBulbOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waterChangeOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockingOverviewPB)).EndInit();
            this.controllerOverviewGB.ResumeLayout(false);
            this.controllerOverviewGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.devicesOverviewGB.ResumeLayout(false);
            this.devicesOverviewGB.PerformLayout();
            this.parametersOverviewGB.ResumeLayout(false);
            this.parametersOverviewGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tempOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ammoniaOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nitriteOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nitrateOverviewPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pHOverviewPB)).EndInit();
            this.pondTP.ResumeLayout(false);
            this.feedingPondGB.ResumeLayout(false);
            this.feedingPondGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.feedTimePondTrB)).EndInit();
            this.metadataPondGB.ResumeLayout(false);
            this.metadataPondGB.PerformLayout();
            this.techInfoPondGB.ResumeLayout(false);
            this.techInfoPondGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fishSuitablePondPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.recFoodPondPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.linerSizePondPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uvRequiredPondPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turnoverRatePondPB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeOverviewPondPB)).EndInit();
            this.dimensionsPondGB.ResumeLayout(false);
            this.dimensionsPondGB.PerformLayout();
            this.fishTP.ResumeLayout(false);
            this.addLivestockFishGB.ResumeLayout(false);
            this.addLivestockFishGB.PerformLayout();
            this.genderFishGB.ResumeLayout(false);
            this.genderFishGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableFishDGV)).EndInit();
            this.parametersTP.ResumeLayout(false);
            this.addRecordParametersGB.ResumeLayout(false);
            this.addRecordParametersGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableParametersDGV)).EndInit();
            this.foodTP.ResumeLayout(false);
            this.addFoodGB.ResumeLayout(false);
            this.addFoodGB.PerformLayout();
            this.sizeFoodGB.ResumeLayout(false);
            this.sizeFoodGB.PerformLayout();
            this.typeFoodGB.ResumeLayout(false);
            this.typeFoodGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableFoodDGV)).EndInit();
            this.powerTP.ResumeLayout(false);
            this.typeDeviceGB.ResumeLayout(false);
            this.typeDeviceGB.PerformLayout();
            this.statusDeviceGB.ResumeLayout(false);
            this.statusDeviceGB.PerformLayout();
            this.scheduleDeviceGB.ResumeLayout(false);
            this.scheduleDeviceGB.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl navigationTC;
        private System.Windows.Forms.TabPage fishTP;
        private System.Windows.Forms.TabPage overviewTP;
        private System.Windows.Forms.DataGridView tableFishDGV;
        private System.Windows.Forms.Button addFishBT;
        private System.Windows.Forms.TextBox ageFishTB;
        private System.Windows.Forms.TabPage parametersTP;
        private System.Windows.Forms.DateTimePicker dateParametersDTP;
        private System.Windows.Forms.TextBox khParametersTB;
        private System.Windows.Forms.TextBox ghParametersTB;
        private System.Windows.Forms.TextBox phParametersTB;
        private System.Windows.Forms.TextBox nitrateParametersTB;
        private System.Windows.Forms.TextBox nitriteParametersTB;
        private System.Windows.Forms.TextBox ammoniaParametersTB;
        private System.Windows.Forms.Button addRecordParametersBT;
        private System.Windows.Forms.DataGridView tableParametersDGV;
        private System.Windows.Forms.Label speciesLB;
        private System.Windows.Forms.Label ageFishLB;
        private System.Windows.Forms.Label ghParametersLB;
        private System.Windows.Forms.Label khParametersLB;
        private System.Windows.Forms.Label nitrateParametersLB;
        private System.Windows.Forms.Label nitriteParametersLB;
        private System.Windows.Forms.Label ammoniaParametersLB;
        private System.Windows.Forms.Label phParametersLB;
        private System.Windows.Forms.Label dateParametersLB;
        private System.Windows.Forms.TabPage foodTP;
        private System.Windows.Forms.Label commentsFoodLB;
        private System.Windows.Forms.Label nameFoodLB;
        private System.Windows.Forms.TextBox commentsFoodTB;
        private System.Windows.Forms.Button addFoodBT;
        private System.Windows.Forms.TextBox nameFoodTB;
        private System.Windows.Forms.RadioButton proteinFoodRB;
        private System.Windows.Forms.RadioButton stapleFoodRB;
        private System.Windows.Forms.RadioButton wheatgermFoodRB;
        private System.Windows.Forms.RadioButton sticksFoodRB;
        private System.Windows.Forms.RadioButton pelletFoodRB;
        private System.Windows.Forms.RadioButton flakeFoodRB;
        private System.Windows.Forms.GroupBox typeFoodGB;
        private System.Windows.Forms.GroupBox sizeFoodGB;
        private System.Windows.Forms.GroupBox parametersOverviewGB;
        private System.Windows.Forms.Label ammoniaOverviewLB;
        private System.Windows.Forms.Label nitrateOverviewLB;
        private System.Windows.Forms.Label pHOverviewLB;
        private System.Windows.Forms.Label nitriteOverviewLB;
        private System.Windows.Forms.TabPage powerTP;
        private System.Windows.Forms.GroupBox statusDeviceGB;
        private System.Windows.Forms.CheckBox scheduledDeviceChB;
        private System.Windows.Forms.RadioButton onDeviceRB;
        private System.Windows.Forms.RadioButton offDeviceRB;
        private System.Windows.Forms.GroupBox scheduleDeviceGB;
        private System.Windows.Forms.TextBox finishMinDeviceTB;
        private System.Windows.Forms.TextBox startMinDeviceTB;
        private System.Windows.Forms.TextBox finishHrDeviceTB;
        private System.Windows.Forms.TextBox startHrDeviceTB;
        private System.Windows.Forms.TextBox nameDeviceTB;
        private System.Windows.Forms.Button removeDeviceBT;
        private System.Windows.Forms.Button addDeviceBT;
        private System.Windows.Forms.ListBox currentDevicesLB;
        private System.Windows.Forms.Label nameDeviceLB;
        private System.Windows.Forms.Label finishDeviceLB;
        private System.Windows.Forms.Label startDeviceLB;
        private System.Windows.Forms.Button loadDeviceBT;
        private System.Windows.Forms.GroupBox devicesOverviewGB;
        private System.Windows.Forms.Label device3OverviewLB;
        private System.Windows.Forms.Label device1OverviewLB;
        private System.Windows.Forms.Label device2OverviewLB;
        private System.Windows.Forms.Label device4OverviewLB;
        private System.Windows.Forms.Button removeFishBT;
        private System.Windows.Forms.Button removeRecordParametersBT;
        private System.Windows.Forms.Button removeFoodBT;
        private System.Windows.Forms.GroupBox typeDeviceGB;
        private System.Windows.Forms.ComboBox typeDeviceCB;
        private System.Windows.Forms.TabPage pondTP;
        private System.Windows.Forms.Label depthPondLB;
        private System.Windows.Forms.Label widthPondLB;
        private System.Windows.Forms.Label lengthPondLB;
        private System.Windows.Forms.TextBox depthPondTB;
        private System.Windows.Forms.TextBox widthPondTB;
        private System.Windows.Forms.TextBox lengthPondTB;
        private System.Windows.Forms.Label typeDeviceLB;
        private System.Windows.Forms.Label device5OverviewLB;
        private System.Windows.Forms.PictureBox pHOverviewPB;
        private System.Windows.Forms.PictureBox ammoniaOverviewPB;
        private System.Windows.Forms.PictureBox nitriteOverviewPB;
        private System.Windows.Forms.PictureBox nitrateOverviewPB;
        private System.Windows.Forms.Label tempOverviewLB;
        private System.Windows.Forms.PictureBox tempOverviewPB;
        private System.Windows.Forms.GroupBox controllerOverviewGB;
        private System.Windows.Forms.Button disconnectOverviewBT;
        private System.Windows.Forms.Button connectOverviewBT;
        private System.Windows.Forms.Label portOverviewLB;
        private System.Windows.Forms.Label ipOverviewLB;
        private System.Windows.Forms.Label portColonLB;
        private System.Windows.Forms.TextBox portOverviewTB;
        private System.Windows.Forms.TextBox ipOverviewTB;
        private System.Windows.Forms.Label connStatusOverviewLB;
        private System.Windows.Forms.GroupBox maintenanceOverviewGB;
        private System.Windows.Forms.Label uvBulbOverviewLB;
        private System.Windows.Forms.Label waterChangeOverviewLB;
        private System.Windows.Forms.Label stockingOverviewLB;
        private System.Windows.Forms.Label waterTestOverviewLB;
        private System.Windows.Forms.Label filterCleanOverviewLB;
        private System.Windows.Forms.Label pumpCleanOverviewLB;
        private System.Windows.Forms.PictureBox waterTestOverviewPB;
        private System.Windows.Forms.PictureBox filterCleanOverviewPB;
        private System.Windows.Forms.PictureBox pumpCleanOverviewPB;
        private System.Windows.Forms.PictureBox uvBulbOverviewPB;
        private System.Windows.Forms.PictureBox waterChangeOverviewPB;
        private System.Windows.Forms.PictureBox stockingOverviewPB;
        private System.Windows.Forms.Label commentsFishLB;
        private System.Windows.Forms.TextBox commentsFishTB;
        private System.Windows.Forms.Label colon2LB;
        private System.Windows.Forms.Label colonLB;
        private System.Windows.Forms.GroupBox techInfoPondGB;
        private System.Windows.Forms.Label linerSizePondLB;
        private System.Windows.Forms.Label uvRequiredPondLB;
        private System.Windows.Forms.Label turnoverRatePondLB;
        private System.Windows.Forms.Label volumeOverviewPondLB;
        private System.Windows.Forms.GroupBox dimensionsPondGB;
        private System.Windows.Forms.Label recFoodPondLB;
        private System.Windows.Forms.Label fishSuitablePondLB;
        private System.Windows.Forms.GroupBox metadataPondGB;
        private System.Windows.Forms.GroupBox addLivestockFishGB;
        private System.Windows.Forms.GroupBox addRecordParametersGB;
        private System.Windows.Forms.PictureBox fishSuitablePondPB;
        private System.Windows.Forms.PictureBox recFoodPondPB;
        private System.Windows.Forms.PictureBox linerSizePondPB;
        private System.Windows.Forms.PictureBox uvRequiredPondPB;
        private System.Windows.Forms.PictureBox turnoverRatePondPB;
        private System.Windows.Forms.PictureBox volumeOverviewPondPB;
        private System.Windows.Forms.DataGridView tableFoodDGV;
        private System.Windows.Forms.GroupBox addFoodGB;
        private System.Windows.Forms.Label timeOverviewLB;
        private System.Windows.Forms.ComboBox fishCB;
        private System.Windows.Forms.CheckBox breedingPondChB;
        private System.Windows.Forms.CheckBox highSunPondChB;
        private System.Windows.Forms.CheckBox plantedPondChB;
        private System.Windows.Forms.Label typePondLB;
        private System.Windows.Forms.ComboBox typePondCB;
        private System.Windows.Forms.CheckBox growthPondChB;
        private System.Windows.Forms.LinkLabel remindersOverviewLL;
        private System.Windows.Forms.Label minDeviceLB;
        private System.Windows.Forms.Label hourDeviceLB;
        private System.Windows.Forms.GroupBox feedingPondGB;
        private System.Windows.Forms.TextBox eveFeedHrPondTB;
        private System.Windows.Forms.TextBox mornFeedHrPondTB;
        private System.Windows.Forms.TextBox eveFeedMinPondTB;
        private System.Windows.Forms.Label eveFeedPondLB;
        private System.Windows.Forms.Label minutePondLB;
        private System.Windows.Forms.Label hourPondLB;
        private System.Windows.Forms.TextBox mornFeedMinPondTB;
        private System.Windows.Forms.Label mornFeedPondLB;
        private System.Windows.Forms.TextBox temperatureParametersTB;
        private System.Windows.Forms.Label temperatureParametersLB;
        private System.Windows.Forms.CheckBox eveFeedDonePondChB;
        private System.Windows.Forms.CheckBox mornFeedDonePondChB;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label correctionFactorPondLB;
        private System.Windows.Forms.TrackBar feedTimePondTrB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox genderFishGB;
        private System.Windows.Forms.RadioButton unknownFishRB;
        private System.Windows.Forms.RadioButton femaleFishRB;
        private System.Windows.Forms.RadioButton maleFishRB;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button feedNowPondBT;
        private System.Windows.Forms.Label donePondLB;
        private System.Windows.Forms.Button dimensionsPondBT;
        private System.Windows.Forms.Button csvWaterChemistryBT;
        private System.Windows.Forms.TextBox sizeFishTB;
        private System.Windows.Forms.Label sizeFishLB;
        private System.Windows.Forms.Button setTimesPondBT;
    }
}

