import select
import socket
import errno
from socket import error as SocketError
import os
import random
from time import localtime, gmtime, strftime, time, sleep
import RPi.GPIO as GPIO

#setup pin layout mode
GPIO.setmode(GPIO.BCM)

# Set relay pins as output
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)

#define directory of temperature file
temp_sensor = '/sys/bus/w1/devices/28-00000922c085/w1_slave'

#feed method
def Feed(feedtime):
   response = "f" + str(feedtime)
   clientSocket.send(response.encode('ascii'))
   clientSocket.close()

   print("[INFO]: feeding started, " + str(feedtime) + "ms")
   GPIO.output(24, GPIO.HIGH)
   sleep(float(feedtime) / 1000.0)
   print("[INFO]: feeding stopped")
   GPIO.output(24, GPIO.LOW)

#read lines from temperature file
def ReadTempFile():

    f = open(temp_sensor, 'r')
    lines = f.readlines()
    f.close()
    return lines

#parse the raw lines from temperature file
def GetTemperature():

    contents = ReadTempFile()
    while contents[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        contents = ReadTempFile()
    temp_output = contents[1].find('t=')

    if temp_output != -1:
        temp_string = contents[1].strip()[temp_output+2:]
        return float(temp_string) / 1000.0

#power control for external devices
def ConfigureOutputs(devcmd):
    for i in range(1, 5):
        if devcmd[i] == "1":
            if i == 1:
                #print("GPIO 1 on")
                GPIO.output(17, GPIO.LOW)
            elif i == 2:
                #print("GPIO 2 on")
                GPIO.output(27, GPIO.LOW)
            elif i == 3:
                #print("GPIO 3 on")
                GPIO.output(22, GPIO.LOW)
            elif i == 4:
                #print("GPIO 4 on")
                GPIO.output(23, GPIO.LOW)
        elif devcmd[i] == "0":
            if i == 1:
                #print("GPIO 1 off")
                GPIO.output(17, GPIO.HIGH)
            elif i == 2:
                #print("GPIO 2 off")
                GPIO.output(27, GPIO.HIGH)
            elif i == 3:
                #print("GPIO 3 off")
                GPIO.output(22, GPIO.HIGH)
            elif i == 4:
                #print("GPIO 4 off")
                GPIO.output(23, GPIO.HIGH)

#server details
ip = "192.168.0.14"
port = 9652

#setup socket server
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.bind((ip, port))
serverSocket.listen(10)

response = ""

print("\r\nServer running on " + ip + ":" + str(port))

#keep servicing requests
while True:
    try:
        clientSocket, address = serverSocket.accept()
        print("\r\n[CONNECTION]: %s" % str(address))

        r, _, _ = select.select([clientSocket], [], [])
        if r:
            command = clientSocket.recv(24)

            if not command:
                break

            command = command[:-1].decode("ASCII")  # remove the $ delimiter and reformat from byte array to string
            print("[REQUEST]: " + command)  # print the raw command out
            response = ""

            if command == "a":
                response = command
                print("[RESPONSE]: " + response)
            if command[:1] == "d":
                ConfigureOutputs(command)
                response = command
                print("[RESPONSE]: " + response)
            elif command == "lt":
                print("[RESPONSE]: " + strftime("%H:%M", localtime()))
                response = strftime("%H:%M", localtime())
            elif command == "t":
                temp = GetTemperature()
                print("[RESPONSE]: " + str(temp) + "*C")
                response = str(temp)
            elif command == "p":
                ph = float(random.randint(69, 78)) / float(10)
                print("[RESPONSE]: " + str(ph))
                response = str(ph)
            elif command[:-4] == "f":
                duration = command[-4:]
                response = command
                Feed(duration)
                continue

            clientSocket.sendall(response.encode('ascii'))

    except SocketError as e:
        if e.errno != errno.ECONNRESET:
            raise
        pass

clientSocket.close()
